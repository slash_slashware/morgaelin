package co.pixals.morgaelin.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import co.pixals.morgaelin.MorgaelinGame;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(640, 400);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new MorgaelinGame();
        }
}