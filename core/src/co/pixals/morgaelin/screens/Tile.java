package co.pixals.morgaelin.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Tile {
	private int x, y;
	private int x2, y2;
	
	private Texture tileset;
	
	private int width, height;
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	
	public int getX2() {
		return x2;
	}
	
	public int getY2() {
		return y2;
	}
	
	public Texture getTileset() {
		return tileset;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public Tile(int x, int y, int x2, int y2, Texture tileset, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.x2 = x2;
		this.y2 = y2;
		this.tileset = tileset;
		this.width = width;
		this.height = height;
	}
	
	public void drawTile(float delta, SpriteBatch spriteBatch, float x, float y) {
		boolean frame = ((int)GameScreen.frameCounter) % 2 == 1;
		int tilex = frame ? this.getX() : this.getX2();
		int tiley = frame ? this.getY() : this.getY2();
		spriteBatch.draw(this.getTileset(), 
				x, y, 
				this.getWidth(), this.getHeight(),
				tilex * this.getWidth(), tiley * this.getHeight(), this.getWidth(), this.getHeight(), false, false);
		
	}
	
}
