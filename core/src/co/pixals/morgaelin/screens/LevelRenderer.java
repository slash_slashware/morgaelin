package co.pixals.morgaelin.screens;

import co.pixals.morgaelin.ImageData;
import co.pixals.morgaelin.world.CellType;
import co.pixals.morgaelin.world.NPC;
import co.pixals.morgaelin.world.Player;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LevelRenderer {
	private static final int TILE_SIZE = 16;
	private static final int TILE_ZOOM = 2;
	private static final int VIEWPORT_X = 16;
	private static final int VIEWPORT_Y = 16;

	
	private Player player;

    private SpriteBatch spriteBatch;
	
	private int height;
	private boolean cameraDetached;
	private int cameraX, cameraY;
	
	public LevelRenderer(Player player) {
		this.player = player;
		spriteBatch = new SpriteBatch();
	}
	
	public void render(float delta){
		int frame = (int)Math.round(GameScreen.frameCounter);
		spriteBatch.begin();
		CellType[][] surroundings = player.getSurroundings();
		int SPRITE_SIZE = TILE_SIZE * TILE_ZOOM;
		for (int x = 0; x < surroundings.length; x++){
			for (int y = 0; y < surroundings[0].length; y++){
				float baseYPosition = height - SPRITE_SIZE - y * SPRITE_SIZE - VIEWPORT_Y; 
				float baseXPosition = VIEWPORT_X + x * SPRITE_SIZE;
				CellType cellType = surroundings[x][y];
				if (cellType != null){
					int levelx = 0;
					int levely = 0;
					if (this.cameraDetached){
						levelx = this.cameraX + x - Player.SIGHT_RANGE;
						levely = this.cameraY + y - Player.SIGHT_RANGE;
					} else {
						levelx = player.getX() + x - Player.SIGHT_RANGE;
						levely = player.getY() + y - Player.SIGHT_RANGE;
					}
					NPC npc = player.getLevel().getNPC(levelx, levely);
					if (npc != null){
						Tile tile = npc.getAppearance();
						tile.drawTile(delta, spriteBatch, baseXPosition, baseYPosition);
						if (player.getLevel().getSpeaking() == npc && frame % 2 == 0){
							ImageData.getTile("SQUARE").drawTile(delta, spriteBatch, baseXPosition, baseYPosition);
						}
					} else if (cellType.isAnimated()){
						Tile tile = cellType.getTile();
						// Water animation offsets in Y, draw the two parts of the tile
						// Draw the two portions
						spriteBatch.draw(ImageData.tileset, 
								baseXPosition, 
								baseYPosition + (TILE_SIZE - frame) * TILE_ZOOM,
								SPRITE_SIZE, frame * TILE_ZOOM, 
								tile.getX() * SPRITE_SIZE, tile.getY() * SPRITE_SIZE + (TILE_SIZE - frame) * TILE_ZOOM, 
								SPRITE_SIZE, frame * TILE_ZOOM, false, false);
						spriteBatch.draw(ImageData.tileset, 
								baseXPosition, 
								baseYPosition, 
								SPRITE_SIZE, (TILE_SIZE - frame) * TILE_ZOOM, 
								tile.getX() * SPRITE_SIZE, tile.getY() * SPRITE_SIZE, 
								SPRITE_SIZE, (TILE_SIZE - frame) * TILE_ZOOM, false, false);
					} else {
						Tile tile = cellType.getTile();
						tile.drawTile(delta, spriteBatch, baseXPosition, baseYPosition);
					}
				}
			}
		}
		if (this.cameraDetached){
			
		} else {
			spriteBatch.draw(ImageData.tileset, 
					VIEWPORT_X + 5 * SPRITE_SIZE, height - VIEWPORT_Y - SPRITE_SIZE - 5 * SPRITE_SIZE, 
					SPRITE_SIZE, SPRITE_SIZE,
					15 * SPRITE_SIZE, 1 * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE, false, false);
		}
		
		spriteBatch.end();
	}

	public void resize(int width, int height) {
		this.height = height;
	}
	
	public void reattachCamera() {
		player.calculateSurroundings();
		this.cameraDetached = false;
	}

	public void setCameraPosition(int x, int y) {
		this.cameraDetached = true;
		player.moveSpirit(x, y);
		this.cameraX = x;
		this.cameraY = y;
	}
	
	public boolean isCameraDetached() {
		return cameraDetached;
	}
}
