package co.pixals.morgaelin.screens;

import java.util.ArrayList;
import java.util.List;

import co.pixals.morgaelin.ImageData;
import co.pixals.morgaelin.world.Armor.ArmorStack;
import co.pixals.morgaelin.world.Artifact.ArtifactStack;
import co.pixals.morgaelin.world.PartyMember;
import co.pixals.morgaelin.world.Player;
import co.pixals.morgaelin.world.SpendableItem.SpendableItemStack;
import co.pixals.morgaelin.world.Weapon.WeaponStack;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class UIRenderer {
	private static final int TEXTBOX_SIZE = 16;

	private Player player;

    private SpriteBatch spriteBatch;
	private TextPrinter textPrinter;
	private static String[] messageBuffer;
	private static String currentPrompt;
	
	private static boolean promptPlayer = false;
	
	private static PanelSetup currentPanel = PanelSetup.PARTY;
	
	public UIRenderer(Player player) {
		this.player = player;
		spriteBatch = new SpriteBatch();
		//textPrinter = new TextPrinter(ImageData.font, 16, 16, 16);
		textPrinter = new TextPrinter(ImageData.font, 16, 16, 10);
		messageBuffer = new String[12];
		for (int i = 0; i < 12; i++){
			messageBuffer[i] = "";
		}
		promptPlayer();
	}
	
	public static void promptPlayer(){
		promptPlayer = true;
	}
	
	public static void executeAction(){
		promptPlayer = false;
	}
	
	public static void addPrompt(String msg){
		addMessage(((char)16)+msg);
	}
	
	public static void addMessage(String msg){
		if (msg.length() <= 16){
			pushMessage(msg);
			return;
		}
			
		// Do the word wrap rap
		String[] tokens = msg.split(" ");
		List<String> lines = new ArrayList<String>();
		int currentLength = 0;
		String currentLine = "";
		int i = 0;
		for (String token: tokens){
			currentLength += token.length() + 1;
			if (currentLength > TEXTBOX_SIZE + 1){
				// Overflow, let's do a CR
				lines.add(currentLine);
				currentLine = token;
				currentLength = token.length();
			} else {
				if (currentLine.length() == 0)
					currentLine = token;
				else
					currentLine += " " + token;
			}
			if (i == tokens.length - 1){
				lines.add(currentLine);
			}
			i++;
		}
		for (String line: lines){
			pushMessage(line);
		}
	}
	
	private static void pushMessage(String msg){
		for (int i = 10; i >= 0; i--){
			messageBuffer[i+1] = messageBuffer[i]; 
		}
		messageBuffer[0] = msg;
	}

	public void render(float delta){
		spriteBatch.begin();
		spriteBatch.draw(ImageData.ui,0,0);
		textPrinter.printText(7*16, 1 * 16, "South Wind", spriteBatch);
		switch (currentPanel){
		case PARTY:
			renderParty(delta);
			break;
		case PARTY_MEMBER:
			renderPartyMember(delta);
			break;
		case ARMOR:
			renderArmor(delta);
			break;
		case WEAPONS:
			renderWeapons(delta);
			break;
		case ITEMS:
			renderItems(delta);
			break;
		case ARTIFACTS:
			renderArtifacts(delta);
			break;
		}
		
		textPrinter.printText(24*16, 14 * 16, "F:"+pad4(player.getFood()), spriteBatch);
		textPrinter.printText(33*16, 14 * 16, "O:"+pad4(player.getObsidians()), spriteBatch);
		int shownLines = 12;
		int baseLine = 1;
		if (promptPlayer){
			shownLines = 11;
			baseLine = 2;
		}
		for (int i = 0; i < shownLines; i++){
			textPrinter.printText(24*16, (i+baseLine) * 16, messageBuffer[i], spriteBatch);
		}
		// Draw the prompt
		if (promptPlayer){
			textPrinter.printText(24*16, 1 * 16, ((char)16)+"", spriteBatch);
			int promptLocation = 0;
			if (currentPrompt != null){
				textPrinter.printText(25*16, 1 * 16, currentPrompt, spriteBatch);
				promptLocation = currentPrompt.length();
			} 
			ImageData.getTile("PROMPT").drawTile(delta, spriteBatch, (25+promptLocation)*16, 1*16);

		}
		spriteBatch.end();
	}

	private void renderWeapons(float delta) {
		String itemCategory = currentPanel.getTitle();
		int nameLength = (itemCategory.length() / 2)+1;
		textPrinter.printText((31-nameLength)*16, 24 * 16, ((char)16)+itemCategory+((char)17), spriteBatch);
		int line = 0;
		for (WeaponStack weaponStack: player.getWeapons()){
			textPrinter.printText(24*16, (23 - line) * 16, (char)('A'+line)+"-" + weaponStack.getQuantity()+" "+weaponStack.getWeapon().getDescription() , spriteBatch);
			line++;
		}
	}
	
	private void renderArmor(float delta) {
		String itemCategory = currentPanel.getTitle();
		int nameLength = (itemCategory.length() / 2)+1;
		textPrinter.printText((31-nameLength)*16, 24 * 16, ((char)16)+itemCategory+((char)17), spriteBatch);
		int line = 0;
		for (ArmorStack armorStack: player.getArmor()){
			textPrinter.printText(24*16, (23 - line) * 16, (char)('A'+line)+"-" + armorStack.getQuantity()+" "+armorStack.getArmor().getDescription() , spriteBatch);
			line++;
		}
	}
	
	private void renderItems(float delta) {
		String itemCategory = currentPanel.getTitle();
		int nameLength = (itemCategory.length() / 2)+1;
		textPrinter.printText((31-nameLength)*16, 24 * 16, ((char)16)+itemCategory+((char)17), spriteBatch);
		int line = 0;
		for (SpendableItemStack itemStack: player.getItems()){
			textPrinter.printText(24*16, (23 - line) * 16, (char)('A'+line)+"-" + itemStack.getQuantity()+" "+itemStack.getSpendableItem().getDescription() , spriteBatch);
			line++;
		}
	}
	
	private void renderArtifacts(float delta) {
		String itemCategory = currentPanel.getTitle();
		int nameLength = (itemCategory.length() / 2)+1;
		textPrinter.printText((31-nameLength)*16, 24 * 16, ((char)16)+itemCategory+((char)17), spriteBatch);
		int line = 0;
		for (ArtifactStack artifactStack: player.getArtifacts()){
			textPrinter.printText(24*16, (23 - line) * 16, (char)('A'+line)+"-" + artifactStack.getArtifact().getDescription() , spriteBatch);
			line++;
		}
	}

	private static PartyMember selectedPartyMember;
	
	public static void setSelectedPartyMember(PartyMember selectedPartyMember) {
		UIRenderer.selectedPartyMember = selectedPartyMember;
	}
	
	private void renderPartyMember(float delta) {
		int nameLength = (selectedPartyMember.getName().length() / 2)+1;
		textPrinter.printText((31-nameLength)*16, 24 * 16, ((char)16)+selectedPartyMember.getName()+((char)17), spriteBatch);

		textPrinter.printText(24*16, 23 * 16, selectedPartyMember.getSex().getTile()+"", spriteBatch);
		textPrinter.printText(28*16, 23 * 16, selectedPartyMember.getTheClass().getDescription(), spriteBatch);
		textPrinter.printText(38*16, 23 * 16, selectedPartyMember.getStatus().getShort(), spriteBatch);
		textPrinter.printText(24*16, 21 * 16, " MP:"+selectedPartyMember.getMagicPoints(), spriteBatch);
		textPrinter.printText(24*16, 20 * 16, "STR:"+selectedPartyMember.getStrength(), spriteBatch);
		textPrinter.printText(24*16, 19 * 16, "DEX:"+selectedPartyMember.getDexterity(), spriteBatch);
		textPrinter.printText(24*16, 18 * 16, "INT:"+selectedPartyMember.getIntelligence(), spriteBatch);
		textPrinter.printText(24*16, 17 * 16, "W:"+selectedPartyMember.getWeapon().getDescription(), spriteBatch);
		textPrinter.printText(24*16, 16 * 16, "A:"+selectedPartyMember.getArmor().getDescription(), spriteBatch);
		
		textPrinter.printText(32*16, 21 * 16, "LV:"+selectedPartyMember.getLevel(), spriteBatch);
		textPrinter.printText(32*16, 20 * 16, "HP:"+pad4(selectedPartyMember.getHitPoints()), spriteBatch);
		textPrinter.printText(32*16, 19 * 16, "HM:"+pad4(selectedPartyMember.getMaxHitPoints()), spriteBatch);
		textPrinter.printText(32*16, 18 * 16, "EX:"+pad4(selectedPartyMember.getExperiencePoints()), spriteBatch);
	}

	


	private void renderParty(float delta) {
		int j = 0;
		for (PartyMember partyMember: player.getParty()){
			textPrinter.printText(24*16, (23-j) * 16, (j+1)+"-", spriteBatch);
			textPrinter.printText(26*16, (23-j) * 16, partyMember.getName(), spriteBatch);
			textPrinter.printText(35*16, (23-j) * 16, partyMember.getHitPoints()+"", spriteBatch);
			textPrinter.printText(38*16, (23-j) * 16, partyMember.getStatus().getShort(), spriteBatch);
			j++;
		}
	}

	private String pad4(int number) {
		if (number < 10)
			return "000"+number;
		else if (number < 100)
			return "00"+number;
		else if (number < 1000)
			return "0"+number;
		else
			return number+"";
	}

	public static void resetPrompt() {
		currentPrompt = "";
	}
	
	public static void setCurrentPrompt(String prompt) {
		currentPrompt = prompt;
	}
	
	public enum PanelSetup {
		PARTY("Party"),
		PARTY_MEMBER("Stats"),
		WEAPONS("Weapons"),
		ARMOR("Armour"),
		ITEMS("General"),
		ARTIFACTS("Artifacts");
		
		private String title;
		private PanelSetup(String title){
			this.title = title;
		}
		
		public String getTitle() {
			return title;
		}
	}

	public static void setCurrentPanel(PanelSetup currentPanel) {
		UIRenderer.currentPanel = currentPanel;
	}
	
	private final static PanelSetup[] PANEL_ORDER = {
		PanelSetup.PARTY_MEMBER,
		PanelSetup.WEAPONS,
		PanelSetup.ARMOR,
		PanelSetup.ITEMS,
		PanelSetup.ARTIFACTS
	};
	
	public static void switchCurrentPanel(int i) {
		int currentPanelIndex = 0;
		for (; currentPanelIndex < PANEL_ORDER.length; currentPanelIndex++){
			if (PANEL_ORDER[currentPanelIndex] == UIRenderer.currentPanel)
				break;
		}
		currentPanelIndex += i;
		if (currentPanelIndex < 0)
			currentPanelIndex = PANEL_ORDER.length + currentPanelIndex;
		currentPanelIndex %= PANEL_ORDER.length;
		setCurrentPanel(PANEL_ORDER[currentPanelIndex]); 
	}
	
	public static PanelSetup getCurrentPanel() {
		return currentPanel;
	}
	
	public static PartyMember getSelectedPartyMember() {
		return selectedPartyMember;
	}
}
