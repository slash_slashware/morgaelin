package co.pixals.morgaelin.screens;

import co.pixals.morgaelin.controller.GameController;
import co.pixals.morgaelin.world.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen implements Screen, InputProcessor{
	public enum OutputMode {
		MAP,
		COMBAT
	}
	
	private Player player;
	private LevelRenderer levelRenderer;
	private CombatRenderer combatRenderer;
	private UIRenderer uiRenderer;
	private GameController controller;
	
	private static OutputMode outputMode = OutputMode.MAP;
	
	public GameScreen(Player p) {
		this.player = p;
	}
	
	public static float frameCounter;

	@Override
	public void render(float delta) {
		frameCounter += 2.0d * delta;
		frameCounter = frameCounter % 16;
		
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		controller.update(delta);
		uiRenderer.render(delta);
		switch (outputMode){
		case COMBAT:
			combatRenderer.render(delta);
			break;
		case MAP:
			levelRenderer.render(delta);
			break;
		}
	}
	
	public static void setOutputMode(OutputMode outputMode) {
		GameScreen.outputMode = outputMode;
	}

	@Override
	public void resize(int width, int height) {
		levelRenderer.resize(width, height);
		combatRenderer.resize(width, height);
	}

	@Override
	public void show() {
		this.levelRenderer = new LevelRenderer(this.player);
		this.combatRenderer = new CombatRenderer(this.player);
		this.uiRenderer = new UIRenderer(this.player);
		this.controller = new GameController(this.player);
		controller.getMusicDJ().playTrack("devonHouse");

		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
		controller.destroy();
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(null);
	}

	// Input Processor
	
	@Override
	public boolean keyDown(int keycode) {
		controller.keyDown(keycode);
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		controller.keyUp(keycode);
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		controller.keyTyped(character);
		return true;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		return true;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		return true;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}
	
	public LevelRenderer getLevelRenderer() {
		return levelRenderer;
	}
}
