package co.pixals.morgaelin.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FourFramesTile extends Tile {
	protected int x3, y3, x4, y4;
	public FourFramesTile(int x, int y, int x2, int y2, int x3, int y3, int x4, int y4, Texture tileset, int width, int height) {
		super(x, y, x2, y2, tileset, width, height);
		this.x3 = x3;
		this.y3 = y3;
		this.x4 = x4;
		this.y4 = y4;
	}
	
	public void drawTile(float delta, SpriteBatch spriteBatch, float x, float y) {
		int frame = ((int)GameScreen.frameCounter) % 4;
		int tilex = 0;
		int tiley = 0;
		switch (frame){
		case 0:
			tilex = getX();
			tiley = getY();
			break;
		case 1:
			tilex = getX2();
			tiley = getY2();
			break;
		case 2:
			tilex = x3;
			tiley = y3;
			break;
		case 3:
			tilex = x4;
			tiley = y4;
			break;
		}
		spriteBatch.draw(this.getTileset(), 
				x, y, 
				this.getWidth(), this.getHeight(),
				tilex * this.getWidth(), tiley * this.getHeight(), this.getWidth(), this.getHeight(), false, false);
		
	}

}
