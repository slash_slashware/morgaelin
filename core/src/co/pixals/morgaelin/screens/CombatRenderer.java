package co.pixals.morgaelin.screens;

import co.pixals.morgaelin.ImageData;
import co.pixals.morgaelin.sprites.SequentialAnimation;
import co.pixals.morgaelin.world.CellType;
import co.pixals.morgaelin.world.CombatArea;
import co.pixals.morgaelin.world.Combatant;
import co.pixals.morgaelin.world.Player;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class CombatRenderer {
	private static final int TILE_SIZE = 16;
	private static final int TILE_ZOOM = 2;
	private static final int VIEWPORT_X = 16;
	private static final int VIEWPORT_Y = 16;
	private static SFX currentSFX;
	private static class SFX {
		public SFX(SequentialAnimation tile, int x, int y) {
			this.tile = tile;
			this.x = x;
			this.y = y;
		}
		SequentialAnimation tile;
		int x, y;
	}
	
	private Player player;
	
    private SpriteBatch spriteBatch;
	
	private int height;
	
	public CombatRenderer(Player player) {
		this.player = player;
		spriteBatch = new SpriteBatch();
	}
	
	public void render(float delta){
		int frame = (int)Math.round(GameScreen.frameCounter);
		spriteBatch.begin();
		CombatArea combatArea = player.getCurrentCombatArea();
		CellType[][] map = combatArea.getCombatLevel().getMap();
		int SPRITE_SIZE = TILE_SIZE * TILE_ZOOM;
		for (int x = 0; x < map.length; x++){
			for (int y = 0; y < map[0].length; y++){
				float baseYPosition = height - SPRITE_SIZE - y * SPRITE_SIZE - VIEWPORT_Y; 
				float baseXPosition = VIEWPORT_X + x * SPRITE_SIZE;
				CellType cellType = map[x][y];
				if (cellType != null){
					Combatant combatant = combatArea.getCombatantAt(x, y);
					if (combatant != null){
						Tile tile = combatant.getAppearance();
						tile.drawTile(delta, spriteBatch, baseXPosition, baseYPosition);
						if (combatant == combatArea.getCurrentCombatant() && frame % 2 == 0){
							ImageData.getTile("SQUARE").drawTile(delta, spriteBatch, baseXPosition, baseYPosition);
						}
					} else if (cellType.isAnimated()){
						Tile tile = cellType.getTile();
						// Water animation offsets in Y, draw the two parts of the tile
						// Draw the two portions
						spriteBatch.draw(ImageData.tileset, 
								baseXPosition, 
								baseYPosition + (TILE_SIZE - frame) * TILE_ZOOM,
								SPRITE_SIZE, frame * TILE_ZOOM, 
								tile.getX() * SPRITE_SIZE, tile.getY() * SPRITE_SIZE + (TILE_SIZE - frame) * TILE_ZOOM, 
								SPRITE_SIZE, frame * TILE_ZOOM, false, false);
						spriteBatch.draw(ImageData.tileset, 
								baseXPosition, 
								baseYPosition, 
								SPRITE_SIZE, (TILE_SIZE - frame) * TILE_ZOOM, 
								tile.getX() * SPRITE_SIZE, tile.getY() * SPRITE_SIZE, 
								SPRITE_SIZE, (TILE_SIZE - frame) * TILE_ZOOM, false, false);
					} else {
						Tile tile = cellType.getTile();
						tile.drawTile(delta, spriteBatch, baseXPosition, baseYPosition);
					}
				}
			}
		}
		// Draw SFX
		if (currentSFX != null){
			float baseYPosition = height - SPRITE_SIZE - currentSFX.y * SPRITE_SIZE - VIEWPORT_Y; 
			float baseXPosition = VIEWPORT_X + currentSFX.x * SPRITE_SIZE;
			
			currentSFX.tile.drawTile(delta, spriteBatch, baseXPosition, baseYPosition);
			if (!currentSFX.tile.active)
				currentSFX = null;
		}
		spriteBatch.end();
	}

	public void resize(int width, int height) {
		this.height = height;
	}

	public static void draw(SequentialAnimation tile, int nx, int ny) {
		currentSFX = new SFX(tile, nx, ny);
		currentSFX.tile.startAnimation();
	}
}
