package co.pixals.morgaelin.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TextPrinter {
	private Texture font;
	private int fontWidth, fontHeight;
	private int charactersWidth;
	
	public TextPrinter(Texture font, int fontWidth, int fontHeight,
			int charactersWidth) {
		super();
		this.font = font;
		this.fontWidth = fontWidth;
		this.fontHeight = fontHeight;
		this.charactersWidth = charactersWidth;
	}

	public void printText(float x, float y, String text, SpriteBatch batch){
		for (int i = 0; i < text.length(); i++){
			char acter = text.charAt(i);
			int index = (int)acter;
			// 97 to 122 is lowercase
			// 65 to 90 is uppercase
			// 49 tp 57 are numb3rs
			int xpos = index % charactersWidth;
			int ypos = (int) Math.floor(index/charactersWidth);
			batch.draw(font, 
					x+i*fontWidth, 
					y,
					fontWidth, fontHeight, 
					xpos * fontWidth, ypos * fontHeight, 
					fontWidth, fontHeight, false, false);
		}
	}
}
