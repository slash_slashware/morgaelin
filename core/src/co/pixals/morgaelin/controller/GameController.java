package co.pixals.morgaelin.controller;

import co.pixals.morgaelin.MusicDJ;
import co.pixals.morgaelin.cutscene.CutsceneData;
import co.pixals.morgaelin.world.NPC;
import co.pixals.morgaelin.world.Player;

public class GameController {
	public enum InputMode {
		MAP_MOVEMENT ,
		CONVERSATION,
		PARTY_MEMBER_SELECTION,
		ITEM_SELECTION,
		ZTATS,
		COMBAT, 
		DIRECTION_SELECTION,
		CUTSCENE
	}
	
	private GameControllerDelegate currentDelegate;

	private MusicDJ musicDJ;

	private Player player;

	public void setInputMode(InputMode inputMode) {
		setInputMode(inputMode, null);
	}

	public void setInputMode(InputMode inputMode, Command command) {
 		switch (inputMode){
		case MAP_MOVEMENT:
			currentDelegate = new MovementGameController(this, player);
			break;
		case CONVERSATION:
			currentDelegate = new ConversationGameController(this, player);
			break;
		case PARTY_MEMBER_SELECTION:
			currentDelegate = new PartyMemberSelectionGameController(this, player);
			break;
		case ZTATS:
			currentDelegate = new ZtatsGameController(this, player);
			break;
		case ITEM_SELECTION:
			currentDelegate = new ItemSelectionGameController(this, player);
			break;
		case COMBAT:
			currentDelegate = new CombatGameController(this, player);
			break;
		case DIRECTION_SELECTION:
			currentDelegate = new DirectionGameController(this, player, command);
			break;
		case CUTSCENE:
			currentDelegate = new CutsceneGameController(this);
			break;
		}
		currentDelegate.reset();
	}
	
	public GameController(Player player) {
		this.player = player;
		musicDJ = new MusicDJ();
		setInputMode(InputMode.MAP_MOVEMENT);
		player.setGameController(this);
	}
	
	public MusicDJ getMusicDJ() {
		return musicDJ;
	}
	
	public void update(float delta){
		currentDelegate.update(delta);
	}
	
	public void keyDown(int keycode) {
		currentDelegate.keyDown(keycode);
	}

	public void keyUp(int keycode) {
		currentDelegate.keyUp(keycode);
	}

	public void keyTyped(char character) {
		currentDelegate.keyTyped(character);
	}

	public void destroy() {
		musicDJ.destroy();
	}

	public void startConversation(NPC npc) {
		setInputMode(InputMode.CONVERSATION);
		((ConversationGameController)currentDelegate).setConversationNPC(npc);
	}
	
	public void startCutscene(CutsceneData cutscene){
		setInputMode(InputMode.CUTSCENE);
		((CutsceneGameController)currentDelegate).setContext(cutscene.steps, player.getLevel());
		
	}
}
