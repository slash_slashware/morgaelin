package co.pixals.morgaelin.controller;

import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.screens.UIRenderer.PanelSetup;
import co.pixals.morgaelin.world.Player;

public class PartyMemberSelectionGameController implements GameControllerDelegate {
	private int selectedMember = -1;
	private Player player;
	private GameController parent;
	
	public PartyMemberSelectionGameController(GameController parent, Player player) {
		this.player = player;
		this.parent = parent;
	}

	@Override
	public void update(float delta) {
		if (selectedMember == -1)
			return;
		UIRenderer.setSelectedPartyMember(player.getParty().get(selectedMember-1));
		UIRenderer.setCurrentPanel(PanelSetup.PARTY_MEMBER);
		UIRenderer.setCurrentPrompt("");
		UIRenderer.addMessage(((char)16)+"Ztats for: "+selectedMember);
		UIRenderer.executeAction();
		parent.setInputMode(InputMode.ZTATS);
	}

	@Override
	public void reset() {
		selectedMember = -1;
	}

	@Override
	public void keyDown(int keycode) {
		// Ignore
	}

	@Override
	public void keyUp(int keycode) {
		// Ignore
	}

	@Override
	public void keyTyped(char character) {
		if (character < '1' || character > '8')
			return;
		int aSelectedMember = Integer.parseInt(character+"");
		if (aSelectedMember <= player.getParty().size()){
			selectedMember = aSelectedMember;
		}
		
	}
	
}
