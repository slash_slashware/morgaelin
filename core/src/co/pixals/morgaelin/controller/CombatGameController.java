package co.pixals.morgaelin.controller;

import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.world.Combatant;
import co.pixals.morgaelin.world.Player;

import com.badlogic.gdx.Input.Keys;

public class CombatGameController implements GameControllerDelegate {
	private boolean up, down, left, right;
	private Player player;
	private long lastTime;
	private boolean alreadyPressed;
	private boolean keyPressed;
	
	private GameController parent;
	
	public CombatGameController(GameController parent, Player player){
		this.parent = parent;
		this.player = player;
	}
	
	private void keyPressed() {
		keyPressed = true;
	}
	
	private void keyReleased() {
		keyPressed = false;
		alreadyPressed = false;
	}
	
	public void leftPressed() {
		left = true;
		keyPressed();
	}

	public void rightPressed() {
		right = true;
		keyPressed();
	}

	public void upPressed() {
		up = true;
		keyPressed();
	}

	public void downPressed() {
		down = true;
		keyPressed();
	}

	public void leftReleased() {
		left = false;
		keyReleased();
	}

	public void rightReleased() {
		right = false;
		keyReleased();
	}

	public void upReleased() {
		up = false;
		keyReleased();
	}

	public void downReleased() {
		down = false;
		keyReleased();
	}
	
	public void update(float delta) {
		UIRenderer.promptPlayer();
		if (!keyPressed)
			return;
		// We update each 300 millis, else we ignore the update
		long current = System.currentTimeMillis(); //TODO: Use delta instead of this
		if (alreadyPressed && current - lastTime < 300){
			return;
		}
		
		alreadyPressed = true;
		lastTime = System.currentTimeMillis();
		
		Combatant currentCombatant = player.getCurrentCombatArea().getCurrentCombatant();
		int dx = 0, dy = 0;
		if (up){
			dx = 0;
			dy = -1;
		} else if (down){
			dx = 0;
			dy = 1;
		} else if (left) {
			dx = -1;
			dy = 0;
		} else if (right) {
			dx = 1;
			dy = 0;
		}
		player.getCurrentCombatArea().tryMoveCombatant(currentCombatant, dx, dy);
		player.getCurrentCombatArea().nextCombatant();
		currentCombatant = player.getCurrentCombatArea().getCurrentCombatant();
		while (!currentCombatant.isPlayerParty()){
			currentCombatant.actInBattle(player.getCurrentCombatArea());
			player.getCurrentCombatArea().nextCombatant();
			currentCombatant = player.getCurrentCombatArea().getCurrentCombatant();
		}
		UIRenderer.addMessage(currentCombatant.getName()+", armed with "+currentCombatant.getWeapon().getDescription());

		
	}

	public void keyDown(int keycode) {
		if (keycode == Keys.LEFT)
			leftPressed();
		if (keycode == Keys.RIGHT)
			rightPressed();
		if (keycode == Keys.UP)
			upPressed();
		if (keycode == Keys.DOWN)
			downPressed();
	}

	public void keyUp(int keycode) {
		if (keycode == Keys.LEFT)
			leftReleased();
		if (keycode == Keys.RIGHT)
			rightReleased();
		if (keycode == Keys.UP)
			upReleased();
		if (keycode == Keys.DOWN)
			downReleased();
	}

	public void reset() {
		alreadyPressed = keyPressed = up = down = left = right = false;
		lastTime = 0L;
	}
	
	@Override
	public void keyTyped(char character) {
		// Do nothing, this controller works on keypresses
	}
}
