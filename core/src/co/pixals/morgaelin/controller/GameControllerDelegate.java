package co.pixals.morgaelin.controller;

public interface GameControllerDelegate {	
	public void update(float delta);

	public void reset();

	public void keyDown(int keycode);

	public void keyUp(int keycode);

	public void keyTyped(char character);
	
}
