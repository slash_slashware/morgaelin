package co.pixals.morgaelin.controller;

import java.util.List;

import com.badlogic.gdx.Input.Keys;

import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.cutscene.DialogStep;
import co.pixals.morgaelin.cutscene.Step;
import co.pixals.morgaelin.world.Level;

public class CutsceneGameController implements GameControllerDelegate {
	private GameController parent;
	public CutsceneGameController(GameController parent) {
		this.parent = parent;
	}
	
	private List<Step> steps;
	private Level level;
	private int currentStepIndex;
	private Step currentStep;

	public void setContext(List<Step> steps, Level level) {
		this.level = level;
		this.steps = steps;
		this.currentStepIndex = -1;
		this.nextStep();
	}
	
	public void nextStep(){
		this.currentStepIndex++;
		this.currentStep = this.steps.get(this.currentStepIndex);
		currentStep.setLevel(level);
		level.setSpeaking(null);
		currentStep.execute();
		if (currentStep.autoForward()){
			if (this.currentStepIndex == this.steps.size()-1){
				// Over.
				parent.setInputMode(InputMode.MAP_MOVEMENT);
			} else {
				nextStep();
			}
		}
	}
	
	@Override
	public void keyTyped(char character){
		
	}
	
	@Override
	public void update(float delta){
		currentStep.update(this, delta);
	}
	
	@Override
	public void keyDown(int keycode) {
		if (currentStep instanceof DialogStep){
			if (keycode == Keys.ENTER){
				nextStep();
			}
		}
	}
	
	@Override
	public void keyUp(int keycode) {
		// Nothing, this delegate is based on key types
	}
	
	@Override
	public void reset() {
		// Nothing to reset
	}

}
