package co.pixals.morgaelin.controller;

import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.world.Direction;
import co.pixals.morgaelin.world.NPC;
import co.pixals.morgaelin.world.Player;

import com.badlogic.gdx.Input.Keys;

public class DirectionGameController implements GameControllerDelegate {
	private Direction selectedDirection = null;
	private Player player;
	private GameController parent;
	private Command command;
	
	public DirectionGameController(GameController parent, Player player, Command command) {
		this.player = player;
		this.parent = parent;
		this.command = command;
	}

	@Override
	public void update(float delta) {
		if (selectedDirection == null)
			return;
		switch (command){
		case ATTACK:
			int targetX = player.getX()+selectedDirection.getDx();
			int targetY = player.getY()+selectedDirection.getDy();
			UIRenderer.addPrompt("Attack: Where");
			UIRenderer.addMessage(selectedDirection.getCardinal());
			NPC npc = player.getLevel().getNPC(targetX, targetY);
			if (npc != null){
				player.startCombat(npc);
				UIRenderer.resetPrompt();
				return;
			} else {
				UIRenderer.addMessage("Nothing there.");
				parent.setInputMode(InputMode.MAP_MOVEMENT);
				UIRenderer.resetPrompt();
				return;
			}
		}
	}

	@Override
	public void reset() {
		selectedDirection = null;
	}

	@Override
	public void keyDown(int keycode) {
		// Ignore
		switch (keycode){
		case Keys.UP:
			selectedDirection = Direction.UP;
			break;
		case Keys.DOWN:
			selectedDirection = Direction.DOWN;
			break;
		case Keys.LEFT:
			selectedDirection = Direction.LEFT;
			break;
		case Keys.RIGHT:
			selectedDirection = Direction.RIGHT;
			break;
		}
	}

	@Override
	public void keyUp(int keycode) {
		// Ignore
	}

	@Override
	public void keyTyped(char character) {
		// Ignore, this one works with keydown
		
	}
	
}
