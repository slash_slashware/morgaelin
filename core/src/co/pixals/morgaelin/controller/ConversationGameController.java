package co.pixals.morgaelin.controller;

import com.badlogic.gdx.Input.Keys;

import co.pixals.morgaelin.PartyMemberData;
import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.world.NPC;
import co.pixals.morgaelin.world.PartyMember;
import co.pixals.morgaelin.world.Player;

public class ConversationGameController implements GameControllerDelegate {
	private Player player;
	private NPC conversationNPC;
	private String currentInterest;
	private char charBuffer;
	private boolean updated = false;
	private boolean enterPressed;
	private boolean backspacePressed;
	private GameController parent;

	
	public ConversationGameController(GameController parent, Player player) {
		this.player = player;
		this.parent = parent;
	}
	public void setConversationNPC(NPC npc) {
		this.conversationNPC = npc;
		UIRenderer.addMessage(((char)16)+"Talk");
		UIRenderer.addMessage("");
		UIRenderer.addMessage(npc.getDescription());
		UIRenderer.addMessage("");
		String greeting = npc.getSex().getGreeting();
		UIRenderer.addMessage(greeting+" "+npc.getGreeting());
		promptInterest();
	}
	
	
	
	private void promptInterest() {
		UIRenderer.addMessage("");
		UIRenderer.addMessage("Your interest:");
		UIRenderer.promptPlayer();
		currentInterest = "";
	}
	
	@Override
	public void keyTyped(char character){
		if (character < 'a' || character > 'z')
			return;
		charBuffer = character;
		updated = true;
	}
	
	@Override
	public void update(float delta){
		if (enterPressed){
			enterPressed = false;
			if (currentInterest.equals(""))
				currentInterest = "bye";
			UIRenderer.addMessage(currentInterest);
			UIRenderer.addMessage("");
			String reply = conversationNPC.ask(currentInterest);
			UIRenderer.addMessage(reply);
			UIRenderer.setCurrentPrompt("");
			if (currentInterest.equals("bye")){
				parent.setInputMode(InputMode.MAP_MOVEMENT);
			} else if (currentInterest.equals("join") && conversationNPC.canJoin()){
				PartyMember partyMember = PartyMemberData.getPartyMember(conversationNPC.getName());
				player.addPartyMember(partyMember);
				player.getLevel().removeNPC(conversationNPC);
				parent.setInputMode(InputMode.MAP_MOVEMENT);
			} else {
				promptInterest();
			}
			return;
		}
		if (backspacePressed){
			backspacePressed = false;
			if (currentInterest.length() > 0)
				currentInterest = currentInterest.substring(0, currentInterest.length()-1);
			UIRenderer.setCurrentPrompt(currentInterest);
			return;
		}
		if (!updated)
			return;
		currentInterest += charBuffer;
		updated = false;
		UIRenderer.setCurrentPrompt(currentInterest);
	}
	
	@Override
	public void keyDown(int keycode) {
		if (updated)
			return; // Characters have higher rank here
		if (keycode == Keys.ENTER){
			enterPressed = true;
		}
		if (keycode == Keys.BACKSPACE){
			backspacePressed = true;
		}
	}
	
	@Override
	public void keyUp(int keycode) {
		// Nothing, this delegate is based on key types
	}
	
	@Override
	public void reset() {
		// Nothing to reset
	}

}
