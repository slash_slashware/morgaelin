package co.pixals.morgaelin.controller;

import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.screens.UIRenderer.PanelSetup;
import co.pixals.morgaelin.world.Player;

import com.badlogic.gdx.Input.Keys;

public class ZtatsGameController implements GameControllerDelegate {
	private boolean leftPressed;
	private boolean rightPressed;
	private GameController parent;

	public ZtatsGameController(GameController parent, Player player) {
		this.parent = parent;
	}

	private float delay;
	private boolean alreadyPressed;
	private boolean escape;
	private boolean use;

	
	@Override
	public void update(float delta) {
		if (escape){
			UIRenderer.setCurrentPanel(PanelSetup.PARTY);
			parent.setInputMode(InputMode.MAP_MOVEMENT);
			return;
		}
		if (use){
			switch (UIRenderer.getCurrentPanel()){
			case ARMOR:
				UIRenderer.addMessage("Wear what?");
				parent.setInputMode(InputMode.ITEM_SELECTION);
				return;
			case WEAPONS:
				UIRenderer.addMessage("Wield what?");
				parent.setInputMode(InputMode.ITEM_SELECTION);
				return;
			case ITEMS: case ARTIFACTS:
				parent.setInputMode(InputMode.ITEM_SELECTION);
				UIRenderer.addMessage("Use what?");
				return;
			default:
				use = false;
				break;
			}
		}
		if (!(leftPressed || rightPressed))
			return;
		delay += delta;
		if (alreadyPressed && delay < 0.3)
			return;
		alreadyPressed = true;
		delay = 0;
		if (leftPressed){
			UIRenderer.switchCurrentPanel(-1);
		} else if (rightPressed){
			UIRenderer.switchCurrentPanel(1);
		}
	}

	@Override
	public void reset() {
		// Ignore
	}

	@Override
	public void keyDown(int keycode) {
		if (keycode == Keys.LEFT){
			leftPressed = true;
		} else if (keycode == Keys.RIGHT){
			rightPressed = true;
		} else if (keycode == Keys.ESCAPE){
			escape = true;
		} else if (keycode == Keys.U){
			use = true;
		}
	}

	@Override
	public void keyUp(int keycode) {
		if (keycode == Keys.LEFT){
			leftPressed = false;
			alreadyPressed = false;			
		} else if (keycode == Keys.RIGHT){
			rightPressed = false;
			alreadyPressed = false;
		}
	}

	@Override
	public void keyTyped(char character) {
		// Ignore
	}

}
