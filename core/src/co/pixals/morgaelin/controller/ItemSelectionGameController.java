package co.pixals.morgaelin.controller;

import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.screens.UIRenderer.PanelSetup;
import co.pixals.morgaelin.world.Armor;
import co.pixals.morgaelin.world.PartyMember;
import co.pixals.morgaelin.world.Player;
import co.pixals.morgaelin.world.Weapon;

public class ItemSelectionGameController implements GameControllerDelegate {
	private int selectedItem = -1;
	private Player player;
	private GameController parent;
	
	public ItemSelectionGameController(GameController parent, Player player) {
		this.player = player;
		this.parent = parent;
	}

	@Override
	public void update(float delta) {
		if (selectedItem == -1)
			return;
		
		PartyMember partyMember = UIRenderer.getSelectedPartyMember();
		switch (UIRenderer.getCurrentPanel()){
		case ARMOR:
			if (partyMember.getArmor() != null && partyMember.getArmor() != Armor.SKIN)
				player.addArmor(partyMember.getArmor(), 1);
			partyMember.wearArmor(player.getArmor().get(selectedItem).getArmor());
			player.getArmor().get(selectedItem).reduce();
			player.checkEmptyStacks();
			break;
		case WEAPONS:
			if (partyMember.getWeapon() != null && partyMember.getWeapon() != Weapon.HANDS)
				player.addWeapons(partyMember.getWeapon(), 1);
			partyMember.wearWeapon(player.getWeapons().get(selectedItem).getWeapon());
			player.getWeapons().get(selectedItem).reduce();
			player.checkEmptyStacks();
			break;
		case ITEMS:
			partyMember.useItem(player.getItems().get(selectedItem).getSpendableItem());
			player.getItems().get(selectedItem).reduce();
			player.checkEmptyStacks();
			break;
		case ARTIFACTS:
			partyMember.useItem(player.getArtifacts().get(selectedItem).getArtifact());
			player.checkEmptyStacks();
			break;
		}
		UIRenderer.setCurrentPanel(PanelSetup.PARTY);
		parent.setInputMode(InputMode.MAP_MOVEMENT);
	}

	@Override
	public void reset() {
		selectedItem = -1;
	}

	@Override
	public void keyDown(int keycode) {
		// Ignore
	}

	@Override
	public void keyUp(int keycode) {
		// Ignore
	}

	@Override
	public void keyTyped(char character) {
		if (character < 'a' || character > 'h')
			return;
		int aSelectedItem = (int)character-(int)'a';
		int theSize = -1;
		switch (UIRenderer.getCurrentPanel()){
		case ARMOR:
			theSize = player.getArmor().size();
			break;
		case ARTIFACTS:
			theSize = player.getArtifacts().size();
			break;
		case ITEMS:
			theSize = player.getItems().size();
			break;
		case WEAPONS:
			theSize = player.getWeapons().size();
			break;
		}
		if (aSelectedItem < theSize){
			selectedItem = aSelectedItem;
		}
		
	}
	
}
