package co.pixals.morgaelin.controller;

import com.badlogic.gdx.Input.Keys;

import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.screens.UIRenderer.PanelSetup;
import co.pixals.morgaelin.world.Player;

public class MovementGameController implements GameControllerDelegate {
	private boolean up, down, left, right;
	private Player player;
	private long lastTime;
	private boolean alreadyPressed;
	private boolean keyPressed;
	
	private boolean zPressed, aPressed;
	private GameController parent;
	
	public MovementGameController(GameController parent, Player player){
		this.parent = parent;
		this.player = player;
	}
	
	private void keyPressed() {
		keyPressed = true;
	}
	
	private void keyReleased() {
		keyPressed = false;
		alreadyPressed = false;
	}
	
	public void leftPressed() {
		left = true;
		keyPressed();
	}

	public void rightPressed() {
		right = true;
		keyPressed();
	}

	public void upPressed() {
		up = true;
		keyPressed();
	}

	public void downPressed() {
		down = true;
		keyPressed();
	}

	public void leftReleased() {
		left = false;
		keyReleased();
	}

	public void rightReleased() {
		right = false;
		keyReleased();
	}

	public void upReleased() {
		up = false;
		keyReleased();
	}

	public void downReleased() {
		down = false;
		keyReleased();
	}
	
	public void update(float delta) {
		UIRenderer.promptPlayer();
		if (zPressed){
			zPressed = false;
			if (player.getParty().size() == 1){
				int selectedPartyMember = 1; 
				UIRenderer.addMessage(((char)16)+"Ztats for: "+selectedPartyMember);
				UIRenderer.setSelectedPartyMember(player.getParty().get(selectedPartyMember-1));
				UIRenderer.setCurrentPanel(PanelSetup.PARTY_MEMBER);
				UIRenderer.executeAction();
				parent.setInputMode(InputMode.ZTATS);
				return;
			} else {
				UIRenderer.setCurrentPrompt("Ztats for:");
				parent.setInputMode(InputMode.PARTY_MEMBER_SELECTION);
				return;
			}
		}
		if (aPressed){
			aPressed = false;
			UIRenderer.setCurrentPrompt("Attack: Where");
			parent.setInputMode(InputMode.DIRECTION_SELECTION, Command.ATTACK);
			return;
		}
		if (!keyPressed)
			return;
		// We update each 300 millis, else we ignore the update
		long current = System.currentTimeMillis(); //TODO: Use delta instead of this
		if (alreadyPressed && current - lastTime < 300){
			return;
		}
		
		alreadyPressed = true;
		lastTime = System.currentTimeMillis();
		if (up){
			player.tryMove(0,-1);
		} else if (down){
			player.tryMove(0,1);
		} else if (left) {
			player.tryMove(-1,0);
		} else if (right) {
			player.tryMove(1,0);
		}
	}

	public void keyDown(int keycode) {
		if (keycode == Keys.LEFT)
			leftPressed();
		if (keycode == Keys.RIGHT)
			rightPressed();
		if (keycode == Keys.UP)
			upPressed();
		if (keycode == Keys.DOWN)
			downPressed();
		if (keycode == Keys.Z)
			zPressed = true;
		if (keycode == Keys.A)
			aPressed = true;
	}

	public void keyUp(int keycode) {
		if (keycode == Keys.LEFT)
			leftReleased();
		if (keycode == Keys.RIGHT)
			rightReleased();
		if (keycode == Keys.UP)
			upReleased();
		if (keycode == Keys.DOWN)
			downReleased();
		if (keycode == Keys.Z)
			zPressed = false;
		if (keycode == Keys.A)
			aPressed = false;
	}

	public void reset() {
		alreadyPressed = keyPressed = up = down = left = right = false;
		lastTime = 0L;
	}
	
	@Override
	public void keyTyped(char character) {
		// Do nothing, this controller works on keypresses
	}
}
