package co.pixals.morgaelin;

import java.util.HashMap;
import java.util.Map;

import co.pixals.morgaelin.screens.Tile;
import co.pixals.morgaelin.world.CellType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

@SuppressWarnings("unchecked")
public class CellTypeData {
	private static Map<String, CellType> cellTypeMap = new HashMap<String, CellType>();
	private static Map<Integer, CellType> cellTypeById = new HashMap<Integer, CellType>();
	public static void init() {
		JsonValue file = new JsonReader().parse(Gdx.files.internal("data/cellTypes.json"));
		JsonValue cellTypes = file.get("cellTypes");
		
		for (int i = 0; i < cellTypes.size; i++){
			JsonValue cellTypeObject = cellTypes.get(i);
			boolean useOffsetAnimation = cellTypeObject.get("useOffsetAnimation") != null && cellTypeObject.getBoolean("useOffsetAnimation");
			boolean isWater = cellTypeObject.get("isWater") != null && cellTypeObject.getBoolean("isWater");
			boolean isSolid = cellTypeObject.get("isSolid") != null && cellTypeObject.getBoolean("isSolid");
			boolean isOpaque = cellTypeObject.get("isOpaque") != null && cellTypeObject.getBoolean("isOpaque");
			String id = cellTypeObject.getString("id");
			int tiledId = cellTypeObject.getInt("tiledId");
			Tile tile = ImageData.getTile(id);
			if (tile == null){
				System.err.println("ERROR: Cell "+id+" has no tile!");
			}
			addCell(id, tile, tiledId, useOffsetAnimation, isWater, isSolid, isOpaque);
		}
	}
	private static void addCell(String code, Tile tile, int tiledId, boolean offsetAnimation, boolean isWater, boolean isSolid, boolean isOpaque) {
		CellType cellType = new CellType();
		cellType.setTile(tile);
		cellType.setAnimated(offsetAnimation);
		cellType.setSolid(isSolid);
		cellType.setWater(isWater);
		cellType.setOpaque(isOpaque);
		cellTypeMap.put(code, cellType);
		cellTypeById.put(tiledId, cellType);
	}
	public static CellType get(String code) {
		return cellTypeMap.get(code);
	}
	
	public static CellType fromIndex(int index) {
		CellType ret = cellTypeById.get(index);
		if (ret != null)
			return ret;
		System.out.println("Error: index "+index+" not loaded");
		return cellTypeById.get(1);
	}

}
