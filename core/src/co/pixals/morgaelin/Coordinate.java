package co.pixals.morgaelin;

public class Coordinate {
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int x, y;
}
