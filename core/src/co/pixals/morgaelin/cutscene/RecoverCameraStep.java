package co.pixals.morgaelin.cutscene;

import co.pixals.morgaelin.MorgaelinGame;

public class RecoverCameraStep extends Step {
	@Override
	public void execute() {
		MorgaelinGame.gameScreen.getLevelRenderer().reattachCamera();
	}
	
	@Override
	public boolean autoForward() {
		return true;
	}
}