package co.pixals.morgaelin.cutscene;

import co.pixals.morgaelin.world.NPC;

public class RemoveCreatureStep extends Step {
	public String person;
	
	@Override
	public void execute() {
		NPC npc = level.getNPC(person);
		level.removeNPC(npc);
	}
	
	@Override
	public boolean autoForward() {
		return true;
	}
}