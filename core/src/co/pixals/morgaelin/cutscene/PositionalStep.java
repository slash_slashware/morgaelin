package co.pixals.morgaelin.cutscene;

public abstract class PositionalStep extends Step{
	public int x;
	public int y;
}