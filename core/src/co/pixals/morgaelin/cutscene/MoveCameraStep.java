package co.pixals.morgaelin.cutscene;

import co.pixals.morgaelin.MorgaelinGame;

public class MoveCameraStep extends PositionalStep {
	@Override
	public void execute() {
		MorgaelinGame.gameScreen.getLevelRenderer().setCameraPosition(x, y);
	}
	
	@Override
	public boolean autoForward() {
		return true;
	}
}