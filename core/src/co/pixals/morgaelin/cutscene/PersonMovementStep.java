package co.pixals.morgaelin.cutscene;

import co.pixals.morgaelin.controller.CutsceneGameController;
import co.pixals.morgaelin.world.NPC;

public class PersonMovementStep extends PositionalStep {
	public String person;
	public boolean disappear;
	private long lastTime;
	private NPC npc;
	
	@Override
	public void execute() {
		npc = level.getNPC(person);
	}
	
	@Override
	public void update(CutsceneGameController cutsceneGameController, float delta) {
		long current = System.currentTimeMillis();
		if (current - lastTime < 600){
			return;
		}
		lastTime = System.currentTimeMillis();
		int xvar = (int) Math.signum(x - npc.getX());
		int yvar = (int) Math.signum(y - npc.getY());
		if (xvar != 0){
			yvar = 0;
		}
		npc.tryMove(xvar, yvar);
		if (npc.getX() == x && npc.getY() == y){
			if (disappear){
				level.removeNPC(npc);
			}
			cutsceneGameController.nextStep();
		}
	}
}