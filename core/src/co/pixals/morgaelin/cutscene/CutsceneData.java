package co.pixals.morgaelin.cutscene;

import java.util.List;

public class CutsceneData {
	public String id;
	public String name;
	public List<Step> steps;
}
