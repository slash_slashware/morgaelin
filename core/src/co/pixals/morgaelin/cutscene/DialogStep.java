package co.pixals.morgaelin.cutscene;

import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.world.NPC;

public class DialogStep extends Step {
	public String shownAs;
	public String person;
	public String dialog;
	private NPC npc;
	
	@Override
	public void execute() {
		npc = level.getNPC(person);
		level.setSpeaking(npc);
		UIRenderer.addMessage("");
		if (shownAs == null) {
			shownAs = person;
		}
		UIRenderer.addMessage(shownAs+": "+dialog);
		UIRenderer.setCurrentPrompt("<Enter>");
	}
}