package co.pixals.morgaelin.cutscene;

import co.pixals.morgaelin.controller.CutsceneGameController;
import co.pixals.morgaelin.world.Level;

public abstract class Step {
	protected Level level;
	
	public void setLevel(Level level) {
		this.level = level;
	}
	
	public abstract void execute();
	
	public boolean autoForward(){
		return false;
	}
	
	public void update(CutsceneGameController cutsceneGameController, float delta) {
		
	}
}