package co.pixals.morgaelin;

import com.badlogic.gdx.Game;

import co.pixals.morgaelin.screens.GameScreen;
import co.pixals.morgaelin.world.Armor;
import co.pixals.morgaelin.world.Artifact;
import co.pixals.morgaelin.world.Level;
import co.pixals.morgaelin.world.PartyMember;
import co.pixals.morgaelin.world.Player;
import co.pixals.morgaelin.world.SpendableItem;
import co.pixals.morgaelin.world.Weapon;

public class MorgaelinGame extends Game {
	
	public static GameScreen gameScreen;
	
	@Override
	public void create () {
		ImageData.init();
		CellTypeData.init();
		PartyMemberData.init();
		
		Level level = LevelData.load("devonHouse");
		Player player = new Player();
		PartyMember theAvatar = PartyMemberData.getPartyMember("Avatar");
		theAvatar.setName("Slash");
		player.addPartyMember(theAvatar);
		player.addFood(5);
		player.addWeapons(Weapon.DAGGER, 5);
		player.addWeapons(Weapon.SWORD, 5);
		player.addArmor(Armor.LEATHER, 2);
		player.addItem(SpendableItem.RED_POTION, 3);
		player.addArtifact(Artifact.HEART_OF_EARTH, 1);
		
		player.setLevel(level);
		level.locatePlayer(player);
		//player.calculateSurroundings();
		gameScreen = new GameScreen(player); 
		setScreen(gameScreen);
	}
}
