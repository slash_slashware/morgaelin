package co.pixals.morgaelin.sprites;

import java.util.List;

import co.pixals.morgaelin.Coordinate;
import co.pixals.morgaelin.screens.GameScreen;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * A n frames animation intended to be displayed only once, and in
 * a defined sequence
 *
 */
public class SequentialAnimation {
	private List<Coordinate> coordinates;
	private int frames;
	private int width;
	private Texture tileset;
	private int height;
	public SequentialAnimation(List<Coordinate> coordinates, Texture tileset, int width, int height) {
		this.coordinates = coordinates;
		this.frames = coordinates.size();
		this.tileset = tileset;
		this.width = width;
		this.height = height;
	}
	
	public boolean active = false;
	private int endingFrame;
	
	public void startAnimation(){
		int currentFrame = ((int)GameScreen.frameCounter);
		endingFrame = (currentFrame + frames) % 16;
		active = true;
	}
	public void drawTile(float delta, SpriteBatch spriteBatch, float x, float y) {
		if (!active)
			return;
		int frame = ((int)GameScreen.frameCounter);
		if (frame == endingFrame ){
			active = false;
			return;
		}
		frame = (frame - endingFrame + frames) % 15;
		int tilex = coordinates.get(frame).x;
		int tiley = coordinates.get(frame).y;
		spriteBatch.draw(tileset, x, y, width, height, tilex * width, tiley * height, width, height, false, false);
		
	}

}
