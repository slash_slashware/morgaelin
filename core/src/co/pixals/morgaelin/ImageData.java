package co.pixals.morgaelin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import co.pixals.morgaelin.screens.FourFramesTile;
import co.pixals.morgaelin.screens.Tile;
import co.pixals.morgaelin.sprites.SequentialAnimation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

@SuppressWarnings("unchecked")
public class ImageData {
	public static Texture ui;
	public static Texture font;
	public static Texture tileset;
	
	private static Map<String, Tile> tiles = new HashMap<String, Tile>();

	public static void init() {
		ui = new Texture(Gdx.files.internal("images/ui.png"));
		//font = new Texture(Gdx.files.internal("images/font.png"));
		font = new Texture(Gdx.files.internal("images/u4ega-charset.png"));
		tileset = new Texture(Gdx.files.internal("images/u8tiles2x.png"));

		JsonValue file = new JsonReader().parse(Gdx.files.internal("data/tiles.json"));
		JsonValue cellTypes = file.get("tiles");
		
		for (int i = 0; i < cellTypes.size; i++){
			JsonValue cellTypeObject = cellTypes.get(i);
			String id = cellTypeObject.getString("id");
			int x = cellTypeObject.getInt("x");
			int y = cellTypeObject.getInt("y");
			int x2 = x;
			int y2 = y;
			if (cellTypeObject.get("x2") != null){
				x2 = cellTypeObject.getInt("x2");
				y2 = cellTypeObject.getInt("y2");
			}
			addTile(id, x, y, x2, y2);
		}
		
		// Special tiles
		Tile prompt = new FourFramesTile(1,3,0,3,9,2,8,2, font, 16, 16);
		tiles.put("PROMPT", prompt);
		
		HIT_SFX = new SequentialAnimation(Arrays.asList(new Coordinate[]{new Coordinate(13,4), new Coordinate(15,4)}), tileset, 32, 32);

	}

	private static void addTile(String id, int x, int y, int x2, int y2) {
		Tile t = new Tile(x-1, y-1, x2-1, y2-1, tileset, 32, 32);
		tiles.put(id, t);
	}

	public static Tile getTile(String id) {
		return tiles.get(id);
	}

	public static SequentialAnimation HIT_SFX;
	
}
