package co.pixals.morgaelin;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MusicDJ {
	
	private Music currentTrack;
	private String currentTrackId = "";
	private Map<String, String> files;
	
	public MusicDJ() {
		files = new HashMap<String, String>();
		files.put("devonHouse", "music/05_-_the_docks.mp3");
		files.put("tenebrae", "music/09_-_tenebrae.mp3");
		files.put("morgaelin", "music/11_-_west_wilderness.mp3");
	}
	
	public void playTrack(String trackId){
		if (trackId.equals(currentTrackId)){
			currentTrack.stop();
			currentTrack.play();
		} else {
			if (currentTrack != null){
				currentTrack.stop();
				currentTrack.dispose();
				currentTrackId = "";
			}
			if (getMP3(trackId) == null)
				return;
			currentTrack = Gdx.audio.newMusic(Gdx.files.internal(getMP3(trackId)));
			currentTrack.setLooping(true);
			currentTrack.setVolume(0.5f);
			currentTrack.play();
			currentTrackId = trackId;
		}
	}
	
	private String getMP3(String trackId) {
		return files.get(trackId);
	}

	public void destroy(){
		currentTrack.dispose();
	}
}
