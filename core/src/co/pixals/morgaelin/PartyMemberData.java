package co.pixals.morgaelin;

import java.util.HashMap;
import java.util.Map;

import co.pixals.morgaelin.world.Armor;
import co.pixals.morgaelin.world.NPC.Sex;
import co.pixals.morgaelin.world.PartyMember;
import co.pixals.morgaelin.world.PartyMember.PartyMemberClass;
import co.pixals.morgaelin.world.Weapon;

public class PartyMemberData {
	private static  Map<String, PartyMember> partyMembers;
	public static void init(){
		partyMembers = new HashMap<String, PartyMember>();
		PartyMember joseph = new PartyMember("Joseph", PartyMemberClass.FISHERMAN, 100, 5, 200, 10, 8, 4, Weapon.HANDS, Armor.SKIN, Sex.MALE, ImageData.getTile("BARD"));
		partyMembers.put("Joseph", joseph);
		PartyMember devon = new PartyMember("Devon", PartyMemberClass.FISHERMAN, 100, 5, 200, 10, 8, 4, Weapon.HANDS, Armor.SKIN, Sex.MALE, ImageData.getTile("MAGE"));
		partyMembers.put("Devon", devon);
		PartyMember theAvatar = new PartyMember("Avatar", PartyMemberClass.AVATAR, 300, 50, 400, 15, 12, 12, Weapon.SERPENT, Armor.SERPENT, Sex.MALE, ImageData.getTile("BARD"));
		partyMembers.put("Avatar", theAvatar);
	}
	
	public static PartyMember getPartyMember(String id){
		return partyMembers.get(id);
	}
}
