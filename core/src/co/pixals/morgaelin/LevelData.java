package co.pixals.morgaelin;

import co.pixals.morgaelin.cutscene.AddCreatureStep;
import co.pixals.morgaelin.cutscene.AnimationStep;
import co.pixals.morgaelin.cutscene.CutsceneData;
import co.pixals.morgaelin.cutscene.DialogStep;
import co.pixals.morgaelin.cutscene.MoveCameraStep;
import co.pixals.morgaelin.cutscene.PersonMovementStep;
import co.pixals.morgaelin.cutscene.RecoverCameraStep;
import co.pixals.morgaelin.cutscene.RemoveCreatureStep;
import co.pixals.morgaelin.cutscene.SFXStep;
import co.pixals.morgaelin.cutscene.Step;
import co.pixals.morgaelin.cutscene.TalkStep;
import co.pixals.morgaelin.screens.Tile;
import co.pixals.morgaelin.world.CellType;
import co.pixals.morgaelin.world.CombatArea;
import co.pixals.morgaelin.world.Level;
import co.pixals.morgaelin.world.NPC;
import co.pixals.morgaelin.world.NPC.Chat;
import co.pixals.morgaelin.world.NPC.Sex;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

@SuppressWarnings("unchecked")
public class LevelData {
	public static CutsceneData loadCutscene(String cutsceneId){
		JsonValue data = new JsonReader().parse(Gdx.files.internal("data/cutscenes/"+cutsceneId+".json"));
		CutsceneData ret = new CutsceneData();
		ret.id = data.getString("id");
		ret.name = data.getString("name");
		JsonValue stepsData = data.get("steps");
		ret.steps = new ArrayList<Step>();
		for (JsonValue stepDataObj: stepsData){
			JsonValue stepData = stepDataObj;
			String stepType = stepData.getString("type"); 
			if (stepType.equals("moveCamera")){
				ret.steps.add(getMoveCameraStep(stepData));
			} else if (stepType.equals("dialog")){
				ret.steps.add(getDialogStep(stepData));
			} else if (stepType.equals("movement")){
				ret.steps.add(getMovementStep(stepData));
			} else if (stepType.equals("animation")){
				ret.steps.add(getAnimationStep(stepData));
			} else if (stepType.equals("addCreature")){
				ret.steps.add(getAddCreatureStep(stepData));
			} else if (stepType.equals("sfx")){
				ret.steps.add(getSFXStep(stepData));
			} else if (stepType.equals("recoverCamera")){
				ret.steps.add(getRecoverCameraStep(stepData));
			} else if (stepType.equals("talk")){
				ret.steps.add(getTalkStep(stepData));
			} else if (stepType.equals("remove")){
				ret.steps.add(getRemoveCreatureStep(stepData));
			}
		}
		return ret;
	}
	
	private static Step getAddCreatureStep(JsonValue stepData) {
		AddCreatureStep ret = new AddCreatureStep();
		ret.creatureCode = stepData.getString("code");
		ret.x = stepData.getInt("x");
		ret.y = stepData.getInt("y");
		return ret;
	}

	private static Step getTalkStep(JsonValue stepData) {
		TalkStep ret = new TalkStep();
		ret.person = stepData.getString("person");
		return ret;
	}

	private static Step getRecoverCameraStep(JsonValue stepData) {
		return new RecoverCameraStep();
	}

	private static Step getSFXStep(JsonValue stepData) {
		SFXStep ret = new SFXStep();
		ret.sfxCode = stepData.getString("code");
		return ret;
	}

	private static Step getAnimationStep(JsonValue stepData) {
		AnimationStep ret = new AnimationStep();
		ret.person = stepData.getString("person");
		ret.animation = stepData.getString("animation");
		return ret;
	}

	private static PersonMovementStep getMovementStep(JsonValue stepData) {
		PersonMovementStep ret = new PersonMovementStep();
		ret.person = stepData.getString("person");
		ret.x = stepData.getInt("toX");
		ret.y = stepData.getInt("toY");
		ret.disappear = stepData.getBoolean("disappear", false); 
		return ret;
	}

	private static DialogStep getDialogStep(JsonValue stepData) {
		DialogStep ret = new DialogStep();
		ret.dialog = stepData.getString("dialog");
		ret.person = stepData.getString("person");
		try {
			ret.shownAs = stepData.getString("shownAs");
		} catch (IllegalArgumentException iae) {
			// It's ok, shownAs is optional.
		}
		return ret;
	}
	
	private static MoveCameraStep getMoveCameraStep(JsonValue stepData) {
		MoveCameraStep ret = new MoveCameraStep();
		ret.x = stepData.getInt("x");
		ret.y = stepData.getInt("y");
		return ret;
	}

	
	private static RemoveCreatureStep getRemoveCreatureStep(JsonValue stepData) {
		RemoveCreatureStep ret = new RemoveCreatureStep();
		ret.person = stepData.getString("person");
		return ret;
	}


	public static Level load(String levelId){
		JsonValue level = new JsonReader().parse(Gdx.files.internal("data/"+levelId+".json"));
		int width = level.getInt("width");
		int height = level.getInt("height");
		JsonValue data = level.get("layers").get(0).get("data");
		CellType[][] map = new CellType[width][height];
		for (int i = 0; i < data.size; i++ ){
			CellType cell = CellTypeData.fromIndex(data.getInt(i));
			int y = (int)Math.floor(i / width);
			int x = i % width;
			map[x][y] = cell;
		}
		JsonValue info = new JsonReader().parse(Gdx.files.internal("data/"+levelId+"Info.json"));
		int startX = info.getInt("startX");
		int startY = info.getInt("startY");
		Level ret = new Level(map, startX, startY);
		String borderPortal = null;
		
		if (info.has("borderPortal")) {
			borderPortal = info.getString("borderPortal");
		}
		
		JsonValue portals = info.get("portals");
		if (portals != null){
			for (JsonValue portalObject: portals){
				int x = portalObject.getInt("x");
				int y = portalObject.getInt("y");
				String destination = portalObject.getString("destination");
				ret.addPortal(destination, x, y);
			}
		}
		ret.setBorderPortal(borderPortal);
		
		String[] cutsceneIds = info.get("cutscenes") != null ? info.get("cutscenes").asStringArray() : null;
		if (cutsceneIds != null){
			for (String cutsceneId: cutsceneIds){
				ret.addCutscene(loadCutscene(cutsceneId));
			}
		}
		
		JsonValue npcs = info.get("npcs");
		if (npcs != null){
			for (JsonValue npcObject: npcs){
				int x = npcObject.getInt("x");
				int y = npcObject.getInt("y");
				String name = npcObject.getString("name");
				String appearanceId = npcObject.getString("appearance");
				String noAnswer = npcObject.getString("noAnswer");
				String description = npcObject.getString("description");
				String greeting = npcObject.getString("greeting");
				String sex = npcObject.getString("sex");
				Boolean canJoin = npcObject.getBoolean("canJoin");
				int hp = 1;
				if (npcObject.get("hp") != null)
					hp = npcObject.getInt("hp");

				
				Tile appearance = ImageData.getTile(appearanceId);
				NPC npc = new NPC(name, appearance, description, greeting, noAnswer, Sex.fromCode(sex), canJoin, hp);
				ret.addNPC(npc, x, y);
				JsonValue chats = npcObject.get("chats");
				for (JsonValue chatObject: chats){
					String chatMessage = chatObject.getString("chat");
					String key = chatObject.getString("key");
					NPC.Chat chat = new Chat(chatMessage); 
					npc.addChat(key, chat);
				}
			}
		}
		ret.setBorderPortal(borderPortal); // TODO: Remove this? duplicated?
		return ret;
	}

	public static CombatArea getCombatArea(CellType cellType) {
		Level combatLevel = load("grassCombatArea");
		return new CombatArea(combatLevel);
	}
}
