package co.pixals.morgaelin.world;

import co.pixals.morgaelin.screens.Tile;

public class CellType {
	private boolean isAnimated;
	private boolean isSolid;
	private boolean isWater;
	private boolean isOpaque;
	private Tile tile;
	
	public void setTile(Tile tile) {
		this.tile = tile;
	}
	
	public Tile getTile() {
		return tile;
	} 
	

	public boolean isAnimated() {
		return isAnimated;
	}
	
	public void setAnimated(boolean isAnimated) {
		this.isAnimated = isAnimated;
	}
	public void setSolid(boolean isSolid) {
		this.isSolid = isSolid;
	}
	
	public boolean isSolid() {
		return isSolid;
	}
	
	public void setWater(boolean isWater) {
		this.isWater = isWater;
	}
	
	public boolean isWater() {
		return isWater;
	}
	
	public void setOpaque(boolean isOpaque) {
		this.isOpaque = isOpaque;
	}
	
	public boolean isOpaque() {
		return isOpaque;
	}
}
