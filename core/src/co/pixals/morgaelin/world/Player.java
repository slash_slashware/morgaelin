package co.pixals.morgaelin.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.pixals.morgaelin.CellTypeData;
import co.pixals.morgaelin.LevelData;
import co.pixals.morgaelin.controller.GameController;
import co.pixals.morgaelin.controller.GameController.InputMode;
import co.pixals.morgaelin.cutscene.CutsceneData;
import co.pixals.morgaelin.cutscene.Step;
import co.pixals.morgaelin.fov.FOV;
import co.pixals.morgaelin.screens.GameScreen;
import co.pixals.morgaelin.screens.GameScreen.OutputMode;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.world.Armor.ArmorStack;
import co.pixals.morgaelin.world.Artifact.ArtifactStack;
import co.pixals.morgaelin.world.SpendableItem.SpendableItemStack;
import co.pixals.morgaelin.world.Weapon.WeaponStack;

public class Player {
	private CellType[][] surroundingsBuffer;
	private Level level;
	private int x, y;
	public final static int SIGHT_RANGE = 5;
	private GameController gameController;
	private List<PartyMember> party;
	private List<WeaponStack> weapons;
	private List<ArmorStack> armor;
	private List<SpendableItemStack> items;
	private List<ArtifactStack> artifacts;
	private int obsidians;
	private int food;
	
	private CombatArea currentCombatArea;
	private Map<String, Boolean> seenCutscenes = new HashMap<String, Boolean>();

	public void setGameController(GameController gameController) {
		this.gameController = gameController;
	}
	
	// Coordinates in the world map
	private int worldx = -1, worldy;
	private FOV fov;
	
	public Player() {
		surroundingsBuffer = new CellType[11][11];
		fov = new FOV();
		party = new ArrayList<PartyMember>();
		weapons = new ArrayList<WeaponStack>();
		armor = new ArrayList<ArmorStack>();
		items = new ArrayList<SpendableItemStack>();
		artifacts = new ArrayList<ArtifactStack>();
	}
	
	public CellType[][] getSurroundings(){
		return surroundingsBuffer;
	}

	public void setLevel(Level level) {
		this.level = level;
	}
	
	public void setPosition(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public void calculateSurroundings(){
		calculateSurroundings(x, y);
	}

	public void moveSpirit(int x, int y){
		calculateSurroundings(x, y);
	}
	
	private void calculateSurroundings(int x, int y){
		level.resetSeen();
		fov.start(level, x, y, 5);
		for (int runx = x - SIGHT_RANGE, bufferx = 0; runx <= x +SIGHT_RANGE; runx++, bufferx++){
			for (int runy = y - SIGHT_RANGE, buffery = 0; runy <= y +SIGHT_RANGE; runy++, buffery++){
				if (level.isInvalid(runx, runy)){
					surroundingsBuffer[bufferx][buffery] = null;
					continue;
				}
				if (level.isSeen(runx, runy))
					surroundingsBuffer[bufferx][buffery] = level.getCell(runx, runy);
				else
					surroundingsBuffer[bufferx][buffery] = null;
			}
		}
	}
	
	public void resetWorldCoordinates(){
		worldx = -1;
	}
	
	private void changeLevel(Level level){
		level.locatePlayer(this);
		CutsceneData cutscene = level.getEntryCutscene();
		if (cutscene != null && !this.hasSeenCutscene(cutscene)){
			this.seenCutscenes.put(cutscene.id, true);
			gameController.startCutscene(cutscene);
		}
	}

	private boolean hasSeenCutscene(CutsceneData cutscene) {
		return this.seenCutscenes.get(cutscene.id) != null;
	}

	public void tryMove(int dx, int dy) {
		String direction = Direction.getCardinal(dx, dy);
		if (!level.isIntro())
			UIRenderer.addMessage(((char)16)+direction);
		int targetX = x + dx;
		int targetY = y + dy;
		if (level.isInvalid(targetX, targetY)){
			if (level.getBorderPortal() != null){
				gameController.getMusicDJ().playTrack(level.getBorderPortal());
				level = LevelData.load(level.getBorderPortal());
				if (worldx == -1){
					changeLevel(level);
				} else {
					setPosition(worldx, worldy);
					calculateSurroundings();
				}
				return;
			} else {
				return;
			}
		}
		String targetPortal = level.getPortalAt(targetX, targetY);
		if (targetPortal != null){
			level = LevelData.load(targetPortal);
			gameController.getMusicDJ().playTrack(targetPortal);
			worldx = x + dx;
			worldy = y + dy;
			changeLevel(level);
			if (targetPortal.equals("devonHouse"))
				resetWorldCoordinates();
			return;
		}
		CellType targetCell = level.getCell(targetX, targetY);
		if (targetCell.isSolid()){
			return;
		}
		if (targetCell.isWater()){
			return;
		}
		NPC npc = level.getNPC(targetX, targetY);
		if (npc != null){
			if (npc.isFriendly())
				gameController.startConversation(npc);
			else
				startCombat(npc);
			return;
		}
		
		x += dx;
		y += dy;
		spendFood();
		level.makeNPCsAct();
		calculateSurroundings();
	}

	public void startCombat(NPC npc) {
		CellType cellType = getLevel().getCell(npc.getX(), npc.getY());
		CombatArea combatArea = LevelData.getCombatArea(cellType);
		// Add party
		int px = 5, py = 8;
		for (PartyMember partyMember: party){
			combatArea.addCombatant(partyMember, px, py);
			px += 2;
		}

		px = 5;
		py = 2;

		// Add enemies
		if (npc.isWarband()){
			for (Combatant c: npc.getWarband()){
				combatArea.addEnemy(c, px, py);
				px += 2;
			}
		} else {
			combatArea.addEnemy(npc, px, py);
		}
		setCurrentCombatArea(combatArea);
		Combatant currentCombatant = combatArea.getCurrentCombatant();
		UIRenderer.addMessage("* * COMBAT! * *");
		UIRenderer.addMessage(currentCombatant.getName()+", armed with "+currentCombatant.getWeapon().getDescription());
		GameScreen.setOutputMode(OutputMode.COMBAT);
		gameController.setInputMode(InputMode.COMBAT);
	}

	private int foodClock = 80;
	private void spendFood() {
		foodClock --;
		if (foodClock < 0){
			foodClock = 80;
			food -= party.size();
			if (food < 0)
				food = 0;
		}
		if (food == 0){
			if (Math.random() > 0.8d){
				UIRenderer.addMessage("Starving!");
				PartyMember starvingMember = party.get((int) Math.floor(Math.random()*party.size()));
				starvingMember.reduceHP(1);
			}
		}
			
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public Level getLevel() {
		return level;
	}

	public void addPartyMember(PartyMember partyMember) {
		party.add(partyMember);
	}

	public List<PartyMember> getParty() {
		return party;
	}
	
	public int getObsidians() {
		return obsidians;
	}
	
	public int getFood() {
		return food;
	}
	
	public void addFood(int food){
		this.food += food;
	}
	
	public void addObsisians(int obsidians){
		this.obsidians += obsidians;
	}
	
	public List<WeaponStack> getWeapons() {
		return weapons;
	}
	
	public List<ArmorStack> getArmor() {
		return armor;
	}
	
	public List<ArtifactStack> getArtifacts() {
		return artifacts;
	}
	
	public List<SpendableItemStack> getItems() {
		return items;
	}

	public void addWeapons(Weapon weapon, int quantity) {
		for (WeaponStack weaponStack: weapons){
			if (weaponStack.getWeapon() == weapon){
				weaponStack.add(quantity);
				return;
			}
		}
		weapons.add(new WeaponStack(weapon, quantity));
	}

	public void addArmor(Armor _armor, int quantity) {
		for (ArmorStack armorStack: armor){
			if (armorStack.getArmor() == _armor){
				armorStack.add(quantity);
				return;
			}
		}
		armor.add(new ArmorStack(_armor, quantity));
	}
	
	public void addItem(SpendableItem item, int quantity) {
		for (SpendableItemStack itemStack: items){
			if (itemStack.getSpendableItem() == item){
				itemStack.add(quantity);
				return;
			}
		}
		items.add(new SpendableItemStack(item, quantity));
	}
	
	public void addArtifact(Artifact item, int quantity) {
		for (ArtifactStack itemStack: artifacts){
			if (itemStack.getArtifact() == item){
				itemStack.add(quantity);
				return;
			}
		}
		artifacts.add(new ArtifactStack(item, quantity));
	}

	public void checkEmptyStacks() {
		for (int i = 0; i < armor.size(); i++){
			if (armor.get(i).getQuantity() == 0){
				armor.remove(i);
				i--;
			}
		}
		for (int i = 0; i < weapons.size(); i++){
			if (weapons.get(i).getQuantity() == 0){
				weapons.remove(i);
				i--;
			}
		}
		for (int i = 0; i < items.size(); i++){
			if (items.get(i).getQuantity() == 0){
				items.remove(i);
				i--;
			}
		}
		for (int i = 0; i < artifacts.size(); i++){
			if (artifacts.get(i).getQuantity() == 0){
				artifacts.remove(i);
				i--;
			}
		}
	}
	
	public void setCurrentCombatArea(CombatArea currentCombatArea) {
		this.currentCombatArea = currentCombatArea;
	}
	
	public CombatArea getCurrentCombatArea() {
		return currentCombatArea;
	}
}