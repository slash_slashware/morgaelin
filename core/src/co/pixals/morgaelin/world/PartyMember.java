package co.pixals.morgaelin.world;

import co.pixals.morgaelin.Util;
import co.pixals.morgaelin.screens.Tile;
import co.pixals.morgaelin.screens.UIRenderer;
import co.pixals.morgaelin.world.NPC.Sex;

public class PartyMember implements Combatant {
	private String name;
	private Tile appearance;
	private Status status;
	private PartyMemberClass theClass;
	private int hitPoints, magicPoints, level, maxHitPoints, maxMagicPoints, experiencePoints, strength, dexterity, intelligence;
	private Weapon weapon;
	private Armor armor;
	private Sex sex;
	
	public PartyMember(String name, PartyMemberClass theClass, int hitPoints,
			int magicPoints, int experiencePoints, int strength, int dexterity,
			int intelligence, Weapon weapon, Armor armor, Sex sex, Tile appearance) {
		this.appearance = appearance;
		this.name = name;
		this.theClass = theClass;
		this.hitPoints = hitPoints;
		this.magicPoints = magicPoints;
		this.experiencePoints = experiencePoints;
		this.strength = strength;
		this.dexterity = dexterity;
		this.intelligence = intelligence;
		this.weapon = weapon;
		this.armor = armor;
		this.sex = sex;
		this.maxHitPoints = hitPoints;
		this.maxMagicPoints = magicPoints;
		this.level = calculateLevel(experiencePoints);
		this.status = Status.GOOD;
		
	}

	private int calculateLevel(int xp) {
		return (int)Math.round(xp / 100.0d); //TODO: Do
	}

	public enum Status {
		GOOD, DEAD;
		
		public String getShort(){
			switch (this){
			case GOOD:
				return "G";
			case DEAD:
				return "D";
			}
			return null;
		}
	}
	
	public enum PartyMemberClass {
		AVATAR ("Avatar"),
		FISHERMAN ("Fisherman");

		private String description;
		
		private PartyMemberClass(String description) {
			this.description = description;
		}
		public String getDescription() {
			return description;
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public int getHitPoints() {
		return hitPoints;
	}
	
	public Status getStatus() {
		return status;
	}

	public void reduceHP(int i) {
		hitPoints -= i;
		if (hitPoints <= 0){
			status = Status.DEAD;
			hitPoints = 0;
		}
	}
	
	public Armor getArmor() {
		return armor;
	}
	
	public int getDexterity() {
		return dexterity;
	}
	
	public int getExperiencePoints() {
		return experiencePoints;
	}
	
	public int getIntelligence() {
		return intelligence;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getMagicPoints() {
		return magicPoints;
	}
	
	public int getMaxHitPoints() {
		return maxHitPoints;
	}
	
	public int getMaxMagicPoints() {
		return maxMagicPoints;
	}
	
	public int getStrength() {
		return strength;
	}
	
	public PartyMemberClass getTheClass() {
		return theClass;
	}
	
	public Weapon getWeapon() {
		return weapon;
	}
	
	public Sex getSex() {
		return sex;
	}

	public void wearArmor(Armor armor) {
		UIRenderer.addMessage(getName()+" switches "+this.armor.getDescription()+" for "+ armor.getDescription());
		this.armor = armor;
		
	}

	public void wearWeapon(Weapon weapon) {
		UIRenderer.addMessage(getName()+" switches "+this.weapon.getDescription()+" for "+ weapon.getDescription());
		this.weapon = weapon;
	}

	public void useItem(SpendableItem item) {
		UIRenderer.addMessage(getName()+" uses "+item.getDescription());
	}

	public void useItem(Artifact artifact) {
		UIRenderer.addMessage(getName()+" uses "+artifact.getDescription());
	}
	
	@Override
	public Tile getAppearance() {
		return appearance;
	}
	
	// Combatant interface, cos every party member should be ready to fight for justice
	private int battleX, battleY;
	@Override
	public int getBattleX() {
		return battleX;
	}
	
	@Override
	public int getBattleY() {
		return battleY;
	}
	
	@Override
	public void locateInBattle(int x, int y) {
		battleX = x;
		battleY = y;
	}
	
	@Override
	public boolean isPlayerParty() {
		return true;
	}
	
	@Override
	public void actInBattle(CombatArea c) {
		// Do Nothing, player controls its party
	}
	
	@Override
	public boolean attack(Combatant enemy) {
		if (getDexterity()+0x80 <= Util.random(0x100)){
			// Miss
			UIRenderer.addMessage("Missed!");
			return false;
		}
		int maxDamage = getWeapon().getDamage();
		maxDamage += getStrength();
		if (maxDamage > 255)
			maxDamage = 255;
		int realDamage = Util.random(maxDamage);
		if (realDamage > 0){
			UIRenderer.addMessage(realDamage+" Damage!");
			enemy.damage(realDamage);
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void damage(int damage) {
		hitPoints -= damage;
		if (hitPoints <= 0){
			hitPoints = 0;
			status = Status.DEAD;
		}
	}
	
	@Override
	public int getDefense() {
		return getArmor().getDefense();
	}
	
	@Override
	public boolean isDead() {
		return status == Status.DEAD;
	}
}
