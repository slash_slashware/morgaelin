package co.pixals.morgaelin.world;

import co.pixals.morgaelin.screens.Tile;

public interface Combatant {
	Tile getAppearance();
	int getBattleX();
	int getBattleY();
	void locateInBattle(int x, int y);
	String getName();
	Weapon getWeapon();
	boolean isPlayerParty();
	void actInBattle(CombatArea combatArea);
	boolean attack(Combatant combatantAt);
	void damage(int damage);
	int getDefense();
	boolean isDead();
}
