package co.pixals.morgaelin.world;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.pixals.morgaelin.ImageData;
import co.pixals.morgaelin.Util;
import co.pixals.morgaelin.screens.CombatRenderer;
import co.pixals.morgaelin.screens.Tile;
import co.pixals.morgaelin.screens.UIRenderer;

public class NPC implements Combatant {
	private String name;
	private Tile apperance;
	private Level level;
	private int x, y;
	private Map<String, Chat> chats;
	private String description;
	private String greeting;
	private String noAnswer;
	private Sex sex;
	private boolean canJoin;

	public NPC(String name, Tile apperance, String description, String greeting, String noAnswer, Sex sex, Boolean canJoin, int hp) {
		this.name = name;
		this.apperance = apperance;
		chats = new HashMap<String, NPC.Chat>();
		this.description = description;
		this.greeting = greeting;
		this.noAnswer = noAnswer;
		this.sex = sex;
		this.canJoin = canJoin;
		this.baseHP = hp;
		this.hp = hp;
	}
	
	public void addChat(String key, Chat chat){
		chats.put(key, chat);
	}
	
	public Chat getChat(String key){
		return chats.get(key);
	}
	
	public String getName() {
		return name;
	}

	public Tile getAppearance() {
		return apperance;
	}
	
	public void act(){
		double rnd = Math.random() * 100;
		if (rnd > 20)
			return;
		int direction = (int)Math.floor(Math.random()*4) + 1;
		switch (direction){
		case 1:
			tryMove(1,0);
			break;
		case 2:
			tryMove(-1,0);
			break;
		case 3:
			tryMove(0, 1);
			break;
		case 4:
			tryMove(0,-1);
			break;
		}
	}

	public void tryMove(int dx, int dy) {
		int targetX = x + dx;
		int targetY = y + dy;
		if (level.getPlayer().getX() == targetX && level.getPlayer().getY() == targetY)
			return;
		if (level.isInvalid(targetX, targetY)){
			return;
		}
		CellType targetCell = level.getCell(targetX, targetY);
		if (targetCell.isSolid()){
			return;
		}
		if (targetCell.isWater()){
			return;
		}
		NPC npc = level.getNPC(targetX, targetY);
		if (npc != null){
			return;
		}
		level.moveNPC(this, targetX, targetY);
	}
	
	public void setLevel(Level level) {
		this.level = level;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public static class Chat {
		private String chat;
		
		public String getChat() {
			return chat;
		}

		public Chat(String chat) {
			this.chat = chat;
		}
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getGreeting() {
		return greeting;
	}
	
	public String getNoAnswer() {
		return noAnswer;
	}
	
	public static enum Sex {
		MALE ("He says:", "M",  (char) 11), 
		FEMALE("She says:", "F", (char) 12), 
		OTHER("It says", "O",(char)11);

		private String greeting, short_;
		private char tile;
		
		private Sex(String greeting, String short_, char tile) {
			this.greeting = greeting;
			this.short_ = short_;
			this.tile = tile;
		}
		
		public String getGreeting() {
			return greeting;
		}
		
		public String getShort() {
			return short_;
		}

		public static Sex fromCode(String sex) {
			if (sex.equals("M"))
				return MALE;
			if (sex.equals("F"))
				return FEMALE;
			if (sex.equals("O"))
				return OTHER;
			return null;
		}

		public char getTile() {
			return tile;
		}

	}
	
	public Sex getSex() {
		return sex;
	}

	public String ask(String interest) {
		Chat c = chats.get(interest);
		if (c == null)
			return getNoAnswer();
		else
			return c.getChat();
	}
	
	public boolean canJoin() {
		return canJoin;
	}

	public boolean isFriendly() {
		// return name.equals("Devon");
		return true;
	}

	public boolean isWarband() {
		return false;
	}

	public List<Combatant> getWarband() {
		return null;
	}
	
	// Combatant interface, cos every npc is a potential enemy
	private int battleX, battleY;
	private int baseHP;
	private int hp;
	private Status status; 
	enum Status {
		GOOD, DEAD;
	}
	
	@Override
	public int getBattleX() {
		return battleX;
	}
	
	@Override
	public int getBattleY() {
		return battleY;
	}
	
	@Override
	public void locateInBattle(int x, int y) {
		battleX = x;
		battleY = y;
	}
	
	@Override
	public Weapon getWeapon() {
		return Weapon.HANDS;
	}
	
	@Override
	public boolean isPlayerParty() {
		return false;
	}
	
	@Override
	public void actInBattle(CombatArea c) {
		if (true)return;
		switch (Util.random(3)){
		case 0:
			c.tryMoveCombatant(this, 0, 1); //TODO: Implement AI
			break;
		case 1:
			c.tryMoveCombatant(this, 0, -1); //TODO: Implement AI
			break;
		case 2:
			c.tryMoveCombatant(this, 1, 0); //TODO: Implement AI
			break;
		case 3:
			c.tryMoveCombatant(this, -1, 0); //TODO: Implement AI
			break;
		}
		
	}
	
	@Override
	public boolean attack(Combatant enemy) {
		if (Util.random(0x100) <= enemy.getDefense()){
			UIRenderer.addMessage(getName()+" misses!");
			return false;
		}
		int x = Util.random(getBaseHP() >> 2);
		int damage = (x >> 4) + ((x>>2)	& 0xfc);
		damage += x % 10;
		if (damage > 0){
			UIRenderer.addMessage(getName()+" hits "+enemy.getName()+" for "+damage);
			//TODO: Change for barely wounded, lightly wounded etc.
			enemy.damage(damage);
			return true;
		} else {
			UIRenderer.addMessage(enemy.getName()+" blocks the attack "+damage);
			return false;
		}
	}
	
	@Override
	public int getDefense() {
		return 0;
	}
	
	public int getBaseHP() {
		return baseHP;
	}
	
	@Override
	public void damage(int damage) {
		hp -= damage;
		if (hp <= 0){
			status = Status.DEAD; 
		}
	}
	
	@Override
	public boolean isDead() {
		return status == Status.DEAD;
	}

}
