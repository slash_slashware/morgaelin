package co.pixals.morgaelin.world;

public enum SpendableItem{
	RED_POTION("Red Potion"),
	YELLOW_POTION("Yellow Potion"),
	ORANGE_POTION("Orange Potion"),
	;
	private String description;
	private SpendableItem(String description){
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static class SpendableItemStack{
		private SpendableItem item;
		private int quantity;
		SpendableItemStack(SpendableItem item, int quantity) {
			this.item = item;
			this.quantity = quantity;
		}
		
		public int getQuantity() {
			return quantity;
		}
		
		public SpendableItem getSpendableItem() {
			return item;
		}

		public void add(int quantity) {
			this.quantity += quantity;
		}

		public void reduce() {
			this.quantity --;

		}
	}
}
