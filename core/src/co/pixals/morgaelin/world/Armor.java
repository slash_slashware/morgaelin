package co.pixals.morgaelin.world;

public enum Armor{
	SKIN("Skin", 0),
	SERPENT("Serpent", 20),
	LEATHER("Leather", 1),
	;
	private String description;
	private int defense;
	
	private Armor(String description, int defense){
		this.description = description;
		this.defense = defense;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static class ArmorStack{
		private Armor armor;
		private int quantity;
		ArmorStack(Armor armor, int quantity) {
			this.armor = armor;
			this.quantity = quantity;
		}
		
		public int getQuantity() {
			return quantity;
		}
		
		public Armor getArmor() {
			return armor;
		}

		public void add(int quantity) {
			this.quantity += quantity;
		}

		public void reduce() {
			this.quantity --;
		}
	}

	public int getDefense() {
		return defense;
	}
}
