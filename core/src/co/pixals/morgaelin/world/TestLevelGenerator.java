package co.pixals.morgaelin.world;

import co.pixals.morgaelin.CellTypeData;

public class TestLevelGenerator {
	public static Level createLevel(){
		
		CellType[][] map = new CellType[levelData[0].length()][levelData.length];
		for (int y = 0; y < levelData.length; y++){
			for (int x = 0; x < levelData[0].length(); x++){
				map[x][y] = getCell(levelData[y].charAt(x));
			}
		}
		Level ret = new Level(map, 5, 5);
		return ret;
	}
	
	private static CellType getCell(char charAt) {
		switch (charAt){
		case '0':
			return CellTypeData.get("WATER1");
		case '1':
			return CellTypeData.get("WATER2");
		case '2':
			return CellTypeData.get("WATER3");
		case '4':
			return CellTypeData.get("LAND1");
		case '5':
			return CellTypeData.get("LAND2");
		case '6':
			return CellTypeData.get("LAND3");
		case '7':
			return CellTypeData.get("MUSHR");
		case '8':
			return CellTypeData.get("HILLS");
		case '9':
			return CellTypeData.get("MOUNT");
		}
		return null;
	}

	static String[] levelData = {
		"00000000000000000000000000000",
		"00000000000000000000000000000",
		"00000000000000000000000000000",
		"00000000000000000000000000000",
		"11111111111110000000011111111",
		"55555555555111111111111555555",
		"55555555554555552555555555777",
		"55555556666555552555555555577",
		"55555555465555552555554565557",
		"55998555555555522555544565777",
		"59999888855555525556666555577",
		"99999998885555525555555555555",
	};
}
