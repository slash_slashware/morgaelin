package co.pixals.morgaelin.world;

public enum Weapon{
	HANDS("Hands", 1),
	SERPENT("Serpent", 10),
	DAGGER("Dagger", 2),
	SWORD("Sword", 4)
	;
	private String description;
	private int damage;
	private Weapon(String description, int damage){
		this.description = description;
		this.damage = damage;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static class WeaponStack{
		private Weapon weapon;
		private int quantity;
		WeaponStack(Weapon weapon, int quantity) {
			this.weapon = weapon;
			this.quantity = quantity;
		}
		
		public int getQuantity() {
			return quantity;
		}
		
		public Weapon getWeapon() {
			return weapon;
		}

		public void add(int quantity) {
			this.quantity += quantity;
		}

		public void reduce() {
			this.quantity --;
		}
	}

	public int getDamage() {
		return damage;
	}
	
}
