package co.pixals.morgaelin.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.pixals.morgaelin.cutscene.CutsceneData;
import co.pixals.morgaelin.cutscene.Step;
import co.pixals.morgaelin.fov.FOV.FOVMap;

public class Level implements FOVMap{
	protected CellType[][] map;
	private int startX, startY;
	private String borderPortal;
	private Map<String, String> portals;
	private Map<String, String> fovMap;
	private Map<String, NPC> npcs;
	private Map<String, NPC> npcsByName;
	private List<NPC> npcsList;
	private Player player;
	private List<CutsceneData> cutscenes = new ArrayList<CutsceneData>();
	private NPC speaking;

	public Level(CellType[][] map, int startX, int startY) {
		this.map = map;
		this.startX = startX;
		this.startY = startY;
		portals = new HashMap<String, String>();
		fovMap = new HashMap<String, String>();
		npcs = new HashMap<String, NPC>();
		npcsByName = new HashMap<String, NPC>();
		npcsList = new ArrayList<NPC>();
	}
	
	public void setBorderPortal(String borderPortal) {
		this.borderPortal = borderPortal;
	}
	
	public void addPortal(String destination, int x, int y) {
		this.portals.put(x+"_"+y, destination);
	}

	public boolean isInvalid(int x, int y) {
		if (x < 0 || y < 0 || x >= map.length || y >= map[0].length)
			return true;
		return false;
	}
	
	public String getBorderPortal() {
		return borderPortal;
	}
	
	public String getPortalAt(int x, int y){
		return portals.get(x+"_"+y);
	}
	
	public CellType getCell(int x, int y) {
		return map[x][y];
	}
	
	public void locatePlayer(Player p){
		p.setPosition(startX, startY);
		player = p;
		// Remove NPCs
		for (PartyMember partyMember: p.getParty()){
			for (int i = 0; i < npcsList.size(); i++){
				NPC npc = npcsList.get(i);
				if (partyMember.getName().equals(npc.getName())){
					removeNPC(npc);
					i--;
				}
						
			}
		}
		player.calculateSurroundings();
	}

	// FOV interface
	@Override
	public boolean blockLOS(int x, int y) {
		if (isInvalid(x, y))
			return true;
		return getCell(x, y).isOpaque();
	}

	@Override
	public void setSeen(int x, int y) {
		fovMap.put(x+"_"+y,"*");
	}
	
	public void resetSeen(){
		fovMap.clear();
	}
	
	public boolean isSeen(int x, int y){
		return fovMap.get(x+"_"+y) != null;
	}

	public void addNPC(NPC npc, int x, int y) {
		npcsList.add(npc);
		npcs.put(x+"_"+y, npc);		
		npcsByName.put(npc.getName(), npc);
		npc.setLevel(this);
		npc.setPosition(x, y);
	}
	
	public void moveNPC(NPC npc, int x, int y) {
		npcs.remove(npc.getX()+"_"+npc.getY());
		npcs.put(x+"_"+y, npc);		
		npc.setPosition(x, y);
	}

	public NPC getNPC(int x, int y) {
		return npcs.get(x+"_"+y);		
	}
	
	public void makeNPCsAct(){
		for (NPC npc:npcsList){
			npc.act();
		}
	}
	
	public Player getPlayer() {
		return player;
	}

	public void removeNPC(NPC npc) {
		npcsList.remove(npc);
		npcs.remove(npc.getX()+"_"+npc.getY());		
	}

	private boolean isIntro;
	
	public boolean isIntro() {
		return isIntro;
	}
	
	public void setIntro(boolean isIntro) {
		this.isIntro = isIntro;
	}

	public CellType[][] getMap() {
		return map;
	}

	public void addCutscene(CutsceneData loadCutscene) {
		cutscenes.add(loadCutscene);
	}

	public CutsceneData getEntryCutscene() {
		if (cutscenes.size() == 0)
			return null;
		return cutscenes.get(0);
	}

	public NPC getNPC(String person) {
		return npcsByName.get(person);
	}

	public void setSpeaking(NPC speaking) {
		this.speaking = speaking;
	}

	public NPC getSpeaking() {
		return speaking;
	}
}
