package co.pixals.morgaelin.world;

public enum Direction {
	UP (0, -1),
	DOWN (0, 1),
	LEFT (-1, 0),
	RIGHT (1,0);
	private int dx, dy;
	
	Direction(int dx, int dy){
		this.dx = dx;
		this.dy = dy;
	}
	
	public int getDx() {
		return dx;
	}
	
	public int getDy() {
		return dy;
	}
	
	public String getCardinal(){
		return getCardinal(dx, dy);
	}
	
	public static String getCardinal(int dx, int dy) {
		if (dx == -1)
			return "West";
		else if (dx == 1)
			return "East";
		else if (dy == -1)
			return "North";
		else if (dy == 1)
			return "South";
		return "None";
	}
}
