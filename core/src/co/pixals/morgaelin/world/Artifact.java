package co.pixals.morgaelin.world;

public enum Artifact{
	TEAR_OF_THE_SEAS("Tear of the Seas"),
	BREATH_OF_AIR("Breath of Air"),
	HEART_OF_EARTH("Heart of Earth"),
	;
	private String description;
	private Artifact(String description){
		if (description.length() > 13)
			description = description.substring(0, 13);
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static class ArtifactStack{
		private Artifact item;
		private int quantity;
		ArtifactStack(Artifact item, int quantity) {
			this.item = item;
			this.quantity = quantity;
		}
		
		public int getQuantity() {
			return quantity;
		}
		
		public Artifact getArtifact() {
			return item;
		}

		public void add(int quantity) {
			this.quantity += quantity;
		}
	}
}
