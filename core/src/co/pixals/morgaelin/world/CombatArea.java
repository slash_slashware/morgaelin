package co.pixals.morgaelin.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.pixals.morgaelin.ImageData;
import co.pixals.morgaelin.screens.CombatRenderer;
import co.pixals.morgaelin.screens.UIRenderer;

public class CombatArea {
	private Map<String, Combatant> combatants;
	private List<Combatant> combatantsList;
	private Level combatLevel;
	private int currentCombatantIndex = 0;
		
	public CombatArea(Level combatLevel) {
		this.combatLevel = combatLevel;
		combatants = new HashMap<String, Combatant>();
		combatantsList = new ArrayList<Combatant>();
	}

	public Combatant getCombatantAt(int x, int y) {
		return combatants.get(x+"_"+y);
	}
	
	public void addCombatant(Combatant combatant, int x, int y){
		combatant.locateInBattle(x,y);
		combatants.put(x+"_"+y, combatant);
		combatantsList.add(combatant);
	}

	public Level getCombatLevel() {
		return combatLevel;
	}
	
	public Combatant getCurrentCombatant() {
		if (currentCombatantIndex >= combatantsList.size())
			currentCombatantIndex = 0;
		return combatantsList.get(currentCombatantIndex);
	}
	
	public void tryMoveCombatant(Combatant combatant, int dx, int dy){
		int x = combatant.getBattleX();
		int y = combatant.getBattleY();
		int nx = x + dx;
		int ny = y + dy;
		if (combatant.isPlayerParty())
			UIRenderer.addPrompt(Direction.getCardinal(dx, dy));
		if (nx < 0 || ny < 0 || nx > 10 || ny > 10){
			combatants.remove(x+"_"+y);
			combatantsList.remove(combatant);
			UIRenderer.addMessage(combatant.getName()+" flees.");
		}
		if (getCombatantAt(nx, ny) != null){
			Combatant defender = getCombatantAt(nx, ny); 
			if (combatant.attack(defender)){
				CombatRenderer.draw(ImageData.HIT_SFX, nx, ny);
			}
			if (defender.isDead()){
				if (!defender.isPlayerParty()){
					combatants.remove(nx+"_"+ny);
					combatantsList.remove(defender);
					UIRenderer.addMessage(defender.getName()+" dies.");

				}
			}
		} else {
			combatants.remove(x+"_"+y);
			combatant.locateInBattle(nx, ny);
			combatants.put(nx+"_"+ny, combatant);
			
		}
	}
	
	public void nextCombatant(){
		if (currentCombatantIndex == combatantsList.size()-1)
			currentCombatantIndex = 0;
		else
			currentCombatantIndex ++;
	}

	public void addEnemy(Combatant c, int x, int y) {
		addCombatant(c, x, y);
	}
	
}
