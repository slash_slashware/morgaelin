package co.pixals.morgaelin.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import co.pixals.morgaelin.MorgaelinGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "morgaelin";
		cfg.width = 640;
		cfg.height = 400;
		
		new LwjglApplication(new MorgaelinGame(), cfg);
	}
}
